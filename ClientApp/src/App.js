import React, { Component } from "react";

import { Route, Switch, Redirect } from "react-router-dom";
import Layout from "./components/layout";
import Home from "./components/home";
import LoginForm from "./components/forms/loginForm";
import Logout from "./components/logout";
import NotFound from "./components/notFound";
import ProtectedRoute from "./components/common/protectedRoute";
import auth from "./services/authService";
import { BrowserRouter } from "react-router-dom";
import RegisterTransportRequest from "./components/transportRequests/registerTransportRequest";
import { ToastContainer } from "react-toastify";
import AddressBook from "./components/addressBook";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import service from "./services/apiService";
import ActualPosition from "./components/transporter/actualPosition";
import RequestsHistory from "./components/requestsHistory";
import TransporterHome from "./components/transporter/home";
import TransporterWork from "./components/transporter/work";
import TransporterAcceptRequest from "./components/transporter/acceptRequest";
import TransporterTransportRequestDetail from "./components/transporter/transportRequestDetail";
import ParcelDetail from "./components/transportRequests/parcelDetail";
import "react-toastify/dist/ReactToastify.min.css";
import {
  setCountries,
  setCurrencies,
  setParcelTypes
} from "./redux/actions/enumsActions";

export class App extends Component {
  state = {};

  componentDidMount() {
    const user = auth.getCurrentUser();
    this.setState({ user });

    this.initEnums();
  }

  initEnums = async () => {
    const { enums, setCurrencies, setParcelTypes, setCountries } = this.props;
    if (!enums.countries.loaded) {
      const response = await service.getCountries();
      setCountries(response.data);
    }
    if (!enums.currencies.loaded) {
      const response = await service.getCurrencies();
      setCurrencies(response.data);
    }
    if (!enums.parcelTypes.loaded) {
      const response = await service.getParcelTypes();
      setParcelTypes(response.data);
    }
  };

  render() {
    const { user } = this.state;
    const baseUrl = document
      .getElementsByTagName("base")[0]
      .getAttribute("href");

    return (
      <div className="navbar-wrapper">
        <ToastContainer />
        <BrowserRouter basename={baseUrl}>
          <Layout>
            <Switch>
              <Route path="/login" component={LoginForm} />
              <Route
                path="/ParcelDetail/:request/:parcel"
                render={props => <ParcelDetail {...props} />}
              />

              <ProtectedRoute path="/logout" component={Logout} />
              <ProtectedRoute
                path="/home"
                render={props => <Home {...props} user={user} />}
              />
              <ProtectedRoute path="/driver" component={ActualPosition} />

              <ProtectedRoute
                path="/request/:id"
                component={RegisterTransportRequest}
              />
              <ProtectedRoute
                path="/requestHistory"
                render={props => <RequestsHistory {...props} user={user} />}
              />

              <ProtectedRoute
                exact
                path="/registerParcel"
                component={RegisterTransportRequest}
              />
              <ProtectedRoute
                exact
                path="/registerParcel/:id"
                component={RegisterTransportRequest}
              />

              <ProtectedRoute
                path="/addressBook/:returnUrl/:target"
                component={AddressBook}
              />

              <ProtectedRoute
                path="/transporter/home"
                exact
                component={TransporterHome}
              />
              <ProtectedRoute
                path="/transporter/work"
                exact
                component={TransporterWork}
              />
              <ProtectedRoute
                exact
                path="/transporter/acceptRequest/:id"
                component={TransporterAcceptRequest}
              />
              <ProtectedRoute
                exact
                path="/transporter/detail/:id"
                component={TransporterTransportRequestDetail}
              />

              <Route path="/not-found" component={NotFound} />
              {user && user.transporter && (
                <React.Fragment>
                  <Redirect from="/" exact to="/transporter" />
                  <Redirect from="/transporter/" exact to="/transporter/home" />
                </React.Fragment>
              )}
              {user && !user.transporter && (
                <Redirect from="/" exact to="/home" />
              )}

              {user && <Redirect to="/not-found" />}
              {!user && <Redirect to="/login" />}
            </Switch>
          </Layout>
        </BrowserRouter>
      </div>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setCountries: setCountries,
      setCurrencies: setCurrencies,
      setParcelTypes: setParcelTypes
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    enums: state.enums
  };
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(App);
