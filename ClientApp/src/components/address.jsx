import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { connect } from "react-redux";

class Address extends Form {
  schema = {
    addressID: Joi.string()
      .required()
      .allow("")
      .label("Address Name/ID"),
    phone: Joi.string()
      .required()
      .allow("")
      .label("Phone"),
    email: Joi.string()
      .email()
      .required()
      .label("E-mail"),
    street: Joi.string()
      .required()
      .label("Street"),
    houseNr: Joi.string()
      .required()
      .label("House number"),
    city: Joi.string()
      .required()
      .label("City"),
    zip: Joi.string()
      .required()
      .label("Postal code"),
    country: Joi.string()
      .required()
      .label("City"),
    requestedLoadingTime: Joi.string()
      .required()
      .label("Loading Time")
  };

  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState() {
    return {
      errors: {},
      loadingTime: new Date()
    };
  }

  setAddressFromStore = () => {
    const { id, address } = this.props;
    this.setData(address[id]);
  };

  onBlur = name => {
    const { onBlur } = this.props;
    if (onBlur) onBlur(name);
  };

  generateOutput = () => {
    this.setAddressFromStore();

    return (
      <React.Fragment>
        <div className="row">
          {this.renderInput(
            "addressID",
            "Address Name/ID",
            "text",
            "col-12",
            "col-md-2",
            "col-md-10"
          )}
        </div>
        <div className="row">
          {this.renderInput("email", "E-mail", "email", "col-6")}
          {this.renderInput("phone", "Phone", "phone", "col-6")}
        </div>
        <div className="row">
          {this.renderInput("street", "Street", "text", "col-9 col-md-6")}
          {this.renderInput("houseNr", "Nr.", "text", "col-3 col-md-6")}
        </div>
        <div className="row">
          {this.renderInput(
            "city",
            "City",
            "text",
            "col-12",
            "col-md-2",
            "col-md-4"
          )}
        </div>
        <div className="row">
          {this.renderInput("zip", "Postal code", "text", "col-12 col-md-6")}
        </div>
        <div className="row">
          {this.renderInput("country", "Country", "text", "col-12 col-md-6")}
        </div>
        <div className="row">
          <div className="col-12">
            <h4>GPS</h4>
          </div>
        </div>
        <div className="row">
          {this.renderInput("latitude", "Latitude", "text", "col-6")}
          {this.renderInput("longitude", "Longitude", "text", "col-6")}
        </div>
      </React.Fragment>
    );
  };
  render() {
    return <React.Fragment>{this.generateOutput()}</React.Fragment>;
  }
}

function mapStateToProps(state) {
  return {
    address: state.addresses
  };
}

export default connect(mapStateToProps)(Address);
