import React, { Component } from "react";
import AddressBookTable from "./addressBookTable";
import _ from "loadsh";
import Pagination from "./common/pagination";
import { paginate } from "../utils/paginate";
import SearchBox from "./searchBox";
import { withRouter } from "react-router-dom";
import service from "../services/apiService";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setAddresses } from "../redux/actions/addressBookActions";
import { setAddress, clearAddress } from "../redux/actions/addressActions";
import ParcelRequestForm from "../components/forms/parcelRequestForm";

class AddressBook extends Component {
  state = {
    showAddress: false,
    //data: [],
    currentPage: 1,
    pageSize: 10,
    searchQuery: "",
    genres: [],
    sortColumn: { path: "addressID", order: "asc" }
  };

  newAddress = null;

  async componentDidMount() {
    const { addressBook, setAddresses } = this.props;
    if (!addressBook.loaded) {
      const response = await service.getAddresses();
      setAddresses(response.data);
    }
  }

  handleDelete = async record => {
    const { setAddresses } = this.props;
    const response = await service.removeAddress(record.id);

    setAddresses(response.data);
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleGenreSelect = genre => {
    this.setState({ selectedGenre: genre, searchQuery: "", currentPage: 1 });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  handleSearch = query => {
    this.setState({ searchQuery: query, selectedGenre: null, currentPage: 1 });
  };

  getPagedData = () => {
    const { pageSize, currentPage, searchQuery, sortColumn } = this.state;

    const { addressBook } = this.props;

    const allData = addressBook.addresses;

    let filtered = allData;

    if (searchQuery) {
      filtered = allData.filter(m => {
        let match = false;
        for (const key of Object.keys(m)) {
          if (
            (m[key] || "")
              .toString()
              .toLowerCase()
              .indexOf(searchQuery.toLowerCase()) > -1
          ) {
            match = true;
            break;
          }
        }
        return match;
      });
    }

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    const data = paginate(sorted, currentPage, pageSize);
    return { totalCount: filtered.length, data };
  };

  toggleAddress = () => {
    const { showAddress } = this.state;
    this.setState({ showAddress: !showAddress });
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleNewAddress = (name, address) => {
    const { setAddress } = this.props;
    setAddress(name, address);
  };

  saveNewAddress = async name => {
    const { showAddress } = this.state;
    const { setAddresses, clearAddress, addresses } = this.props;

    console.log("saveNewAddress:" + name, addresses);

    const response = await service.addAddress(addresses[name]);
    this.setState({ data: response.data, showAddress: !showAddress }, () => {
      setAddresses(this.state.data);
      clearAddress(name);
    });
  };

  handleSelect = (name, record) => {
    const { target, returnUrl } = this.props.match.params;
    const { onSelect, setAddress, history } = this.props;

    if (onSelect) onSelect(name, record);
    else {
      setAddress(target, record);
      history.push("/" + returnUrl);
    }
  };

  handleClose = () => {
    const { returnUrl } = this.props.match.params;
    const { history } = this.props;
    history.push("/" + returnUrl);
  };

  render() {
    //const { length: count } = this.state.data;

    //if (count === 0) return <p>There are no records!</p>;

    const {
      pageSize,
      currentPage,
      sortColumn,
      searchQuery,
      showAddress
    } = this.state;
    const { totalCount, data } = this.getPagedData();

    return (
      <React.Fragment>
        {showAddress ? (
          <ParcelRequestForm
            label="New address"
            id="newAddress"
            horizontal
            backToList="true"
            map="true"
            toolbar="false"
            toggle={this.toggleAddress}
            onSave={this.saveNewAddress}
            onChange={this.handleNewAddress}
          />
        ) : (
          <React.Fragment>
            <div className="row">
              <div className="col-lg-6">
                <button
                  onClick={this.toggleAddress}
                  className="btn btn-primary"
                  style={{ marginBottom: 20 }}
                >
                  Add address
                </button>
              </div>
            </div>
            <p>Showing {totalCount} records in database.</p>
            <SearchBox value={searchQuery} onChange={this.handleSearch} />
            <AddressBookTable
              name={this.props.name || ""}
              sortColumn={sortColumn}
              data={data}
              onSort={this.handleSort}
              onDelete={this.handleDelete}
              onSelect={this.handleSelect}
            />
            <Pagination
              itemsCount={totalCount}
              currentPage={currentPage}
              pageSize={pageSize}
              onPageChange={this.handlePageChange}
            />
            <button className="btn btn-primary" onClick={this.handleClose}>
              Close
            </button>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setAddress: setAddress,
      clearAddress: clearAddress,
      setAddresses: setAddresses
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    addressBook: state.addressBook,
    addresses: state.addresses
  };
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(withRouter(AddressBook));

