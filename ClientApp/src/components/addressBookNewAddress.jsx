import Joi from "joi-browser";
import React from "react";
import Form from "./common/form";
import GoogleMapWithSearchBar from "./google/googleMapWithSearchBar";
import addressGoogle from "../dto/addressGoogle";

class AddressBookNewAddress extends Form {
  schema = {
    id: Joi.string().optional(),
    latitude: Joi.string().optional(),
    longitude: Joi.string().optional(),
    search: Joi.string().optional(),
    addressID: Joi.string()
      .required()
      .label("Address ID"),
    street: Joi.string()
      .required()
      .label("Street"),
    number: Joi.string()
      .required()
      .label("House number"),
    city: Joi.string()
      .required()
      .label("City"),
    zipcode: Joi.string()
      .required()
      .label("Postal code"),
    country: Joi.string()
      .required()
      .label("City")
  };

  dto;
  query = "";
  data = {};

  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState() {
    this.dto = new addressGoogle();
    this.data = this.dto.address;
    return {
      errors: {},
      changeHandler: null
    };
  }

  componentDidMount() {
    const { id } = this.props;
    this.setId(id);
  }

  updateState = data => {
    const { update } = this.props;
    update(this.dto.address);
    this.data = data;
  };

  parseInput = (id, address_components, location) => {
    this.dto = new addressGoogle();
    this.dto.parseInput(id, address_components, location);
    this.updateState({ data: this.dto.address });
  };

  updateQuery = query => {
    this.dto.addQuery(query);
    this.updateState({ data: this.dto.address });
  };

  generateOutput = () => {
    const { horizontal } = this.props;

    return horizontal !== undefined ? (
      <div>
        {this.renderHorizontalInput("addressID", "Address ID")}
        {this.renderHorizontalInput("street", "Street")}
        {this.renderHorizontalInput("houseNr", "House number")}
        {this.renderHorizontalInput("city", "City")}
        {this.renderHorizontalInput("zip", "Postal code")}
        {this.renderHorizontalInput("country", "Country")}
      </div>
    ) : (
      <React.Fragment>
        {this.renderInput("addressID", "Address ID")}
        {this.renderInput("street", "Street")}
        {this.renderInput("houseNr", "House number")}
        {this.renderInput("city", "City")}
        {this.renderInput("zip", "Postal code")}
        {this.renderInput("country", "Country")}
      </React.Fragment>
    );
  };
  render() {
    const { toggle, save } = this.props;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-lg-12">
            <h3>Add address</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">{this.generateOutput()}</div>
          <div className="col-lg-6">
            <GoogleMapWithSearchBar
              map
              updateQuery={this.updateQuery}
              parseInput={this.parseInput}
            />
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-sm-6">
            <button
              onClick={toggle}
              className="btn btn-primary"
              style={{ marginBottom: 20 }}
            >
              Back to list
            </button>
          </div>
          <div className="col-sm-6" style={{ textAlign: "right" }}>
            <button className="btn btn-primary" onClick={save}>
              Save
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AddressBookNewAddress;
