import React, { Component } from "react";
import Table from "./common/table";
import auth from "../services/authService";

class AddressBookTable extends Component {
  columns = [
    {
      path: "addressID",
      label: "Address ID",
      content: record => (
        <a
          className="clickable"
          style={{ display: "block" }}
          onClick={() => this.onSelect(record)}
        >
          {record.addressID}
        </a>
      )
    },
    { path: "street", label: "Street", hidden: "md" },
    { path: "houseNr", label: "Nr.", hidden: "md" },
    { path: "city", label: "City", hidden: "md" },
    { path: "zip", label: "Postal code", hidden: "md" },
    { path: "phone", label: "Phone", hidden: "md" },
    { path: "latitude", label: "Latitude", hidden: "md" },
    { path: "longitude", label: "Longitude", hidden: "md" }
  ];

  deleteColumn = {
    style: { width: "50px" },
    key: "delete",
    content: record => (
      <button
        onClick={() => {
          this.props.onDelete(record);
        }}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    )
  };

  constructor() {
    super();
    const user = auth.getCurrentUser();
    if (user) {
      // && user.isAdmin)
      this.columns.push(this.deleteColumn);
    }
  }

  onSelect = record => {
    const { onSelect, name } = this.props;
    if (onSelect) onSelect(name, record);
  };

  render() {
    const { data, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={data}
        onSort={onSort}
        sortColumn={sortColumn}
      />
    );
  }
}

export default AddressBookTable;
