import React, { Component } from "react";
import CheckBoxCore from "@material-ui/core/Checkbox";

class Checkbox extends Component {
  state = { checked: false };

  componentDidMount() {
    const { checked } = this.props;
    if (checked) this.setState({ checked });
  }

  handleChange = name => event => {
    const { onChange, id, name } = this.props;
    const checked = event.target.checked;
    this.setState({ checked }, onChange(id, name, checked));
  };

  render() {
    const { checked } = this.state;
    const { id, disabled } = this.props;

    return (
      <CheckBoxCore
        key={id}
        disabled={disabled || false}
        checked={checked}
        onChange={this.handleChange(id)}
        value={id}
        color="primary"
        inputProps={{
          "aria-label": "primary checkbox"
        }}
      />
    );
  }
}

export default Checkbox;
