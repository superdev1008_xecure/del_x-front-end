import React, { Component } from "react";
import MomentUtils from "@date-io/moment";
import {
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers";

class DateTimePicker extends Component {
  handleDateChange = date => {
    const { onChange, name } = this.props;
    //onChange(name, date);
  };
  render() {
    const { selectedDate, label, disabled } = this.props;

    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <KeyboardDateTimePicker
          valent={"line"}
          ampm={false}
          label={label}
          value={selectedDate}
          onChange={this.handleDateChange}
          //onError={console.log}
          disabled={disabled || false}
          disablePast
          format="DD/MM/YYYY HH:ss"
        />
      </MuiPickersUtilsProvider>
    );
  }
}

export default DateTimePicker;
