import React, { Component } from "react";

class FileUpload extends Component {
  state = {
    imageBase64: null
  };

  componentDidMount() {
    const { image: imageBase64 } = this.props;
    this.setState({ imageBase64 });
  }

  fileChangedHandler = event => {
    if (event.target.files.length === 0) return;

    const { handleSelectFile, id } = this.props;
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageBase64: reader.result
      });

      handleSelectFile(id, reader.result);
    };

    reader.readAsDataURL(event.target.files[0]);
  };

  render() {
    const { imageBase64 } = this.state;
    const { id, filter, disabled, url } = this.props;

    return (
      <React.Fragment>
        {!disabled && (
          <input
            type="file"
            name={`image-${id}`}
            onChange={this.fileChangedHandler}
            accept={filter}
          />
        )}
        <div className="image-container">
          {imageBase64 && <img src={imageBase64} alt="Preview" width="200" />}
          {!imageBase64 && url && disabled && (
            <img src={url} alt="Preview" width="200" />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default FileUpload;
