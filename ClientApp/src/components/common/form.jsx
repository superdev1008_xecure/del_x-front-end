import React, { Component } from "react";
import Joi from "joi-browser";
import Input from "./input";
import Select from "./select";
import addressDTO from "../../dto/addressDTO";

class Form extends Component {
  state = {
    errors: {}
  };

  setData = data => {
    this.data = data || { ...addressDTO };
  };

  validate = () => {
    const options = { abortEarly: false };
    const { error } = Joi.validate(this.data, this.schema, options);

    if (!error) return null;

    const errors = {};
    for (let item of error.details) {
      errors[item.path[0]] = item.message;
    }

    //console.log(errors);
    return errors;
  };

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleSubmit = e => {
    e.preventDefault();

    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;

    if (this.doSubmit) this.doSubmit();
  };

  handleChange = ({ currentTarget: input }) => {
    const { onChange, id } = this.props;
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    this.data[input.name] = input.value;
    this.setState({ errors });

    if (onChange) {
      onChange(id, this.data);
      // console.log("HANDLE_CHANGE", this.data);
    }
  };

  /*
  handleDateChange = (key, selectedDate) => {
    //console.log(key);
    //console.log(selectedDate);
    const { onChange, id } = this.props;
    this.data[key] = selectedDate._d;
    if (onChange) {
      onChange(id, this.data);
      console.log("HANDLE_CHANGE", this.data);
    }
    this.setState({ [key]: selectedDate });
  };

  handleCheckboxChange = (key, value) => {
    console.log(key);
    console.log(value);
    this.setState({ [key]: value });
  };
*/

  renderInput(
    name,
    label,
    type = "text",
    cssClass = "",
    cssClassLabel = "",
    cssClassInput = "",
    editable = true,
    orientation = "vertical"
  ) {
    const { errors } = this.state;
    const { id, readonly, horizontal, defaultValues } = this.props;

    const value = defaultValues ? defaultValues[name] : "";

    return (
      <Input
        readOnly={!editable || readonly || false}
        id={(id || "") + "_" + name}
        error={errors[name]}
        name={name}
        value={this.data[name] || value || ""}
        label={label}
        type={type}
        cssClass={cssClass}
        cssClassLabel={cssClassLabel}
        cssClassInput={cssClassInput}
        orientation={
          horizontal || orientation === "horizontal" ? "horizontal" : ""
        }
        onChange={this.handleChange}
        onBlur={this.onBlur}
      />
    );
  }

  renderSelect(
    name,
    label,
    options,
    cssClass = "",
    cssClassLabel = "",
    cssClassInput = "",
    selectedValue = "",
    readonly = false
  ) {
    const { errors } = this.state;
    const { id } = this.props;
    return (
      <Select
        id={(id || "") + "_" + name}
        error={errors[name]}
        name={name}
        disabled={readonly}
        value={this.data[name] || selectedValue || ""}
        orientation="horizontal"
        cssClass={cssClass}
        cssClassLabel={cssClassLabel}
        cssClassInput={cssClassInput}
        label={label}
        options={options}
        onChange={() => this.handleSelectChange(id, name)}
      />
    );
  }

  renderSubmitButton(label) {
    return (
      <button disabled={this.validate()} className="btn btn-primary">
        {label}
      </button>
    );
  }

  renderButton(label, action, className, style) {
    return (
      <button
        style={style}
        className={`btn  ${className || "btn-primary"}`}
        onClick={() => action(this.state.id)}
      >
        {label}
      </button>
    );
  }

  guidGenerator() {
    var S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (
      S4() +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      S4() +
      S4()
    );
  }
}

export default Form;
