import React from "react";

const Input = ({
  id,
  error,
  label,
  orientation,
  cssClass,
  cssClassLabel,
  cssClassInput,
  onBlur,
  ...rest
}) => {
  return orientation === "horizontal" ? (
    <div className={`form-group ${cssClass ? cssClass : "col-12"}`}>
      <div className="row">
        <label
          className={`col-form-label ${
            cssClassLabel ? cssClassLabel : "col-md-4"
          }`}
          htmlFor={id}
        >
          {label}
        </label>
        <div className={`${cssClassInput ? cssClassInput : "col-md-8"}`}>
          <input
            {...rest}
            id={id}
            onBlur={() => onBlur(id)}
            className="form-control"
          />

        </div>
      </div>
    </div>
  ) : (
    <div className={`form-group ${cssClass}`}>
      <label htmlFor={id}>{label}</label>
      <input {...rest} id={id} className="form-control" />

    </div>
  );
};

export default Input;
