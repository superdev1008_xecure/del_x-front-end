import React, { Component } from "react";
import Input from "./input";

class InputExtended extends Component {
  state = {
    errors: []
  };

  data = {};

  handleChange = ({ currentTarget: input }) => {
    const { onChange, id, name } = this.props;
    if (onChange) onChange(id, name, input.value);
  };

  onBlur = () => {};

  render() {
    const { errors } = this.state;
    const {
      id,
      name,
      label,
      cssClass = "",
      cssClassLabel = "",
      cssClassInput = "",
      orientation = "horizontal",
      ...rest
    } = this.props;

    return (
      <Input
        {...rest}
        id={(id || "") + "_" + name}
        error={errors[name]}
        name={name}
        label={label}
        cssClass={cssClass}
        cssClassLabel={cssClassLabel}
        cssClassInput={cssClassInput}
        orientation={orientation}
        onChange={this.handleChange}
        onBlur={this.onBlur}
      />
    );
  }
}

export default InputExtended;
