import React from "react";
const ListGroup = ({
  items,
  labelProperty,
  valueProperty,
  onItemSelect,
  selectedItem
}) => {
  return (
    <ul className="list-group">
      {items.map(item => (
        <li
          onClick={() => onItemSelect(item)}
          key={item[valueProperty]}
          className={
            selectedItem === item ? "list-group-item active" : "list-group-item"
          }
        >
          {item[labelProperty]}
        </li>
      ))}
    </ul>
  );
};

ListGroup.defaultProps = {
  labelProperty: "name",
  valueProperty: "_id"
};

export default ListGroup;
