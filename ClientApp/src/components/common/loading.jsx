import React, { Component } from "react";
import { DualRing } from "react-loading-io";

const Loading = () => {
  return (
    <React.Fragment>
      <div className="text-center">
        <DualRing size={64} color="#4CA7FD" speed={1.5} width={7} height={15} />{" "}
        <h2>Loading data. Please wait&hellip;</h2>
      </div>
    </React.Fragment>
  );
};

export default Loading;
