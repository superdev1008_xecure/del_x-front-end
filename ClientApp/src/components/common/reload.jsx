import React, { Component } from "react";

class Reloader extends Component {
  state = {};
  started = false;

  startAction = async () => {
    console.log("TICK");
    const { condition, action, timeout } = this.props;
    if (condition) {
      await action();
      setTimeout(
        async () => await this.startAction(condition, action, timeout),
        timeout
      );
    }
  };

  render() {
    const { condition } = this.props;
    if (condition && !this.started) {
      this.started = true;
      this.startAction();
    } else {
      this.started = false;
    }
    return <React.Fragment></React.Fragment>;
  }
}

export default Reloader;
