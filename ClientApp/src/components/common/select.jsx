import React from "react";

const Select = ({
  id,
  error,
  label,
  options,
  orientation,
  cssClass,
  cssClassLabel,
  cssClassInput,
  ...rest
}) => {
  return orientation === "horizontal" ? (
    <div className={`form-group ${cssClass ? cssClass : "col-12"}`}>
      <div className="row">
        <label
          className={`col-form-label ${
            cssClassLabel ? cssClassLabel : "col-md-4"
          }`}
          htmlFor={id}
        >
          {label}
        </label>
        <div className={`${cssClassInput ? cssClassInput : "col-md-8"}`}>
          <select className="form-control" id={id} {...rest}>
            <option value="" />
            {options
              .filter(option => option.value !== null)
              .map(option => (
                <option key={option.value} value={option.value}>
                  {option.label}
                </option>
              ))}
          </select>
          {error && <div className="alert alert-danger">{error}</div>}
        </div>
      </div>
    </div>
  ) : (
    <div className="form-group">
      <label className={cssClassLabel ? cssClassLabel : ""} htmlFor={id}>
        {label}
      </label>
      <select
        className={`${cssClassInput ? cssClassInput : "form-control"}`}
        id={id}
        {...rest}
      >
        <option value="" />
        {options
          .filter(option => option.value !== null)
          .map(option => (
            <option
              key={option.id || option.code}
              value={option.id || option.code}
            >
              {option.label || option.name}
            </option>
          ))}
      </select>
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Select;
