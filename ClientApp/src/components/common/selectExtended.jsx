import React, { Component } from "react";
import Select from "./select";

class SelectExtended extends Component {
  state = {
    errors: []
  };

  data = {};

  handleChange = ({ currentTarget: input }) => {
    const { onChange, id, name } = this.props;
    if (onChange) onChange(id, name, input.value);
  };

  render() {
    const {
      id,
      name,
      label,
      options,
      cssClass = "",
      cssClassLabel = "",
      cssClassInput = "",
      selectedValue = "",
      readonly = false
    } = this.props;

    const { errors } = this.state;

    return (
      <Select
        id={(id || "") + "_" + name}
        error={errors[name]}
        name={name}
        disabled={readonly}
        value={this.data[name] || selectedValue || ""}
        orientation="horizontal"
        cssClass={cssClass}
        cssClassLabel={cssClassLabel}
        cssClassInput={cssClassInput}
        label={label}
        options={options}
        onChange={this.handleChange}
      />
    );
  }
}

export default SelectExtended;
