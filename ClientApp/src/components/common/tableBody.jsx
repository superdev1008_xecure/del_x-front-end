import React, { Component } from "react";
import _ from "loadsh";
class TableBody extends Component {
  renderCell = (item, column) => {
    if (column.content) return column.content(item);
    return _.get(item, column.path);
  };

  createCellKey = (item, column) => {
    return item[this.props.valueProperty] + (column.path || column.key);
  };

  render() {
    const { data, columns, valueProperty } = this.props;
    return (
      <tbody>
        {data.map(item => (
          <tr key={item[valueProperty]}>
            {columns.map(column => (
              <td
                key={this.createCellKey(item, column)}
                className={`${column.class || ""} ${
                  column.hidden
                    ? `d-none d-${column.hidden}-table-cell`
                    : "d-table-cell"
                }`}
              >
                {this.renderCell(item, column)}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    );
  }
}

TableBody.defaultProps = {
  valueProperty: "id"
};

export default TableBody;
