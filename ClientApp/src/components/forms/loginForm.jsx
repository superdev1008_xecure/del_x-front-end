import React from "react";
import { Redirect } from "react-router-dom";
import Joi from "joi-browser";
import Form from "../common/form";
import auth from "../../services/authService";
import { toast } from "react-toastify";

class LoginForm extends Form {
  data = { email: "", password: "" };

  state = {
    errors: {}
  };

  schema = {
    email: Joi.string()
      .required()
      .email()
      .label("E-mail"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      await auth.login(this.data.email, this.data.password);

      const { state } = this.props.location;

      window.location = state ? state.from.pathname : "/";
    } catch (error) {
      toast.warn("Username or password is incorrect.");
      console.log("ERROR", error);
    }
  };

  render() {
    const user = auth.getCurrentUser();
    if (user) {
      if (user.transporter) return <Redirect to="/transporter" />;
      else return <Redirect to="/" />;
    }

    return (
      <div>
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("email", "E-mail")}
          {this.renderInput("password", "Password", "password")}
          {this.renderSubmitButton("Login")}
        </form>
      </div>
    );
  }
}

export default LoginForm;
