import React from "react";
import Form from "../common/form";
import Address from "./../address";
import Parcel from "./../parcel";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";
import GoogleMapWithSearchBar from "../google/googleMapWithSearchBar";
import addressGoogle from "../../dto/addressGoogle";
import { setAddress } from "../../redux/actions/addressActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setAddresses } from "../../redux/actions/addressBookActions";

class ParcelRequestForm extends Form {
  dto = new addressGoogle();

  state = {
    map: false,
    toolbar: true,
    backToList: false
  };

  componentDidMount() {
    const {
      map: mapProp,
      toolbar: mapToolbar,
      backToList: mapBackToList
    } = this.props;

    let { map, toolbar, backToList } = this.state;

    if (mapProp) map = mapProp === "true";
    if (mapToolbar) toolbar = mapToolbar === "true";
    if (mapBackToList) backToList = mapBackToList === "true";
    this.setState({ map, toolbar, backToList });
  }

  toggleMap = () => {
    this.setState({ map: !this.state.map });
  };

  parseInput = (name, id, address_components, location) => {
    const { setAddress, onChange } = this.props;
    this.dto.parseInput(id, address_components, location);
    setAddress(name, this.dto.address);
    onChange(name, this.dto.address);
  };

  updateQuery = (name, query) => {
    this.dto.setQuery(query);
    setAddress(name, this.dto.address);
  };

  updateSearchQuery = name => {
    //this.dto.setQuery(this.dto.getQuery());
  };

  render() {
    const {
      id,
      index,
      onRemove,
      location,
      label,
      addresses,
      toggle,
      onSave,
      onChange,
      readonly,
      defaultValues,
      enums
    } = this.props;

    const { map, toolbar, backToList } = this.state;
    const url = `/addressBook/${location.pathname.split("/")[1]}/${id}`;
    let query = "";
    const address = addresses[id];
    if (address) {
      //query = address.street + ", " + address.city;
    }
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 col-md-8">
            <h2>{label || `Parcel #${index}`}</h2>
          </div>
          <div className="col-12 col-md-4" style={{ textAlign: "right" }}>
            {toolbar && !readonly && (
              <React.Fragment>
                <button
                  className="btn btn-secondary btn-sm"
                  onClick={this.toggleMap}
                  style={{ marginRight: 10 }}
                >
                  {map ? "Hide map" : "Show map"}
                </button>
                <NavLink
                  className="btn btn-primary btn-sm"
                  style={{
                    marginRight: index === 1 ? 0 : 10
                  }}
                  to={url}
                >
                  Address book
                </NavLink>
                {index &&
                  index > 1 &&
                  this.renderButton("Remove", onRemove, "btn-danger btn-sm")}
              </React.Fragment>
            )}
          </div>
        </div>
        <div className="row">
          <div className={`col-lg-${map ? 6 : 12}`}>
            <Address
              readonly={readonly}
              horizontal
              id={id}
              onChange={onChange}
              onBlur={this.updateSearchQuery}
              defaultValues={defaultValues}
            />
          </div>
          {map && (
            <div className="col-lg-6">
              <GoogleMapWithSearchBar
                map
                id={id}
                target={id}
                query={query}
                updateQuery={this.updateQuery}
                parseInput={this.parseInput}
              />
            </div>
          )}
        </div>
        {backToList &&
          this.renderButton("Back to list", toggle, "btn-sm btn-secondary", {
            marginRight: 10
          })}
        {onSave &&
          !readonly &&
          this.renderButton(
            "Save address",
            () => onSave(id),
            "btn-sm btn-primary float-right"
          )}
        <br clear="all" />
        {enums && (
          <Parcel
            readonly={readonly}
            horizontal
            enums={enums}
            id={id}
            onChange={onChange}
            onBlur={this.updateSearchQuery}
          />
        )}
      </React.Fragment>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setAddress: setAddress,
      setAddresses: setAddresses
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    addresses: state.addresses
  };
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(withRouter(ParcelRequestForm));
