import React from "react";
import { connect } from "react-redux";
import Form from "../common/form";
import TransportRequest from "./../../dto/transportRequest";
import Address from "./../address";
import ParcelRequestForm from "./parcelRequestForm";
import ReactModal from "react-modal";
import AddressBook from "../addressBook";
import addressDTO from "../../dto/addressDTO";
import { bindActionCreators } from "redux";
import { setAddress } from "../../actions/addressActions";

ReactModal.setAppElement("#root");

class RegisterParcelForm extends Form {
  request = new TransportRequest();

  schema = {};

  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState = () => {
    return {
      data: { username: "", password: "", name: "" },
      index: 0,
      sender: new addressDTO(),
      requestedLoadingTime: null,
      parcels: [],
      addressBook: false,
      errors: {},
      modalIsOpen: false,

      addresses: [],
      addressesLoaded: false
    };
  };

  openModal = () => {
    this.setState({ modalIsOpen: true });
  };

  afterOpenModal = () => {
    // references are now sync'd and can be accessed.
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };

  componentDidMount() {
    if (this.state.parcels.length === 0) this.addParcel();
  }

  handleAddressFromUpdate = address => {
    this.request.setOrigin(address);
  };

  handleAddressToUpdate = address => {
    this.request.setDestination(address);
  };

  toggleAddressBook = () => {
    this.setState({ addressBook: !this.state.addressBook });
    //this.props.history.push("/registerParcel/addressBook");
  };

  removeParcel = id => {
    const parcels = this.state.parcels.filter(m => m.id !== id);
    const index = this.state.index - 1;

    this.setState({ parcels, index });
  };

  addParcel = () => {
    const parcels = [...this.state.parcels];
    const index = this.state.index + 1;
    const htmlId = "id_" + this.guidGenerator();
    parcels.push({ id: htmlId, index });

    this.setState({ parcels, index });
  };

  handleAddressUpdate = id => {
    console.log("handleAddressUpdate", id);
  };

  handleSelectAddress = (target, id) => {
    const { addresses } = this.state;
    const { setAddress } = this.props;

    const _address = addresses.filter(m => m.id === id);
    if (_address === null || _address.length === 0) return;

    setAddress(target, _address[0]);

    if (target === "sender") this.closeModal();
  };

  updateAddresses = addresses => {
    this.setState({ addresses, addressesLoaded: true });
  };

  saveAddress = async name => {
    const { showAddress } = this.state;
    const { setAddresses, addresses } = this.props;

    const response = await service.addAddress(addresses[name]);
    this.setState({ data: response.data, showAddress: !showAddress }, () => {
      setAddresses(this.state.data);
    });
  };

  render() {
    const { addresses, addressesLoaded } = this.state;
    return (
      <React.Fragment>
        <h1>Register parcel form</h1>
        <form onSubmit={this.handleSubmit}>
          <ReactModal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            contentLabel="Example Modal"
          >
            <h2>Address book</h2>
            <AddressBook
              name="sender"
              onSelect={this.handleSelectAddress}
              listLoaded={addressesLoaded}
              list={addresses}
              onListUpdate={this.updateAddresses}
            />
            {this.renderButton("Close", this.closeModal)}
          </ReactModal>
          <hr />
          {!this.state.addressBook && (
            <React.Fragment>
              <div className="row">
                <div className="col-sm-8">
                  <h2>Sender</h2>
                </div>
                <div className="col-sm-4" style={{ textAlign: "right" }}>
                  {this.renderButton("Address book", this.openModal)}
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <Address
                    horizontal
                    id="sender"
                    update={this.handleAddressUpdate}
                  />
                </div>
              </div>
              {this.state.parcels.map(({ id, index }) => {
                return (
                  <React.Fragment key={id}>
                    <hr />
                    <ParcelRequestForm
                      key={id}
                      index={index}
                      id={id}
                      onRemove={this.removeParcel}
                      onSave={this.saveAddress}
                    />
                  </React.Fragment>
                );
              })}
              <hr />
              {this.renderButton("Add parcel", this.addParcel)}
            </React.Fragment>
          )}
        </form>
      </React.Fragment>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setAddress: setAddress
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    address: state.addresses
  };
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(RegisterParcelForm);
