import React from "react";
import GoogleMapWithSearchBar from "./googleMapWithSearchBar";
import Joi from "joi-browser";
import Form from "../common/form";
import addressDTO from "../../dto/address";

class GoogleMapWithAddress extends Form {
  schema = {
    street: Joi.string()
      .required()
      .label("Street"),
    number: Joi.string()
      .required()
      .label("Number"),
    city: Joi.string()
      .required()
      .label("City"),
    zipcode: Joi.string()
      .required()
      .label("Zipcode"),
    country: Joi.string()
      .required()
      .label("Country"),
    longitude: Joi.string()
      .required()
      .label("Longitude"),
    latitude: Joi.string()
      .required()
      .label("Latitude")
  };

  dto;
  query = "";

  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState = () => {
    this.dto = new addressDTO();
    return {
      data: this.dto.address,
      errors: {}
    };
  };

  updateState = data => {
    const { update } = this.props;
    update(this.dto.address);
    this.setState(data);
  };

  parseInput = (id, address_components, location) => {
    this.dto = new addressDTO();
    this.dto.parseInput(id, address_components, location);
    this.updateState({ data: this.dto.address });
  };

  updateQuery = query => {
    this.dto.addQuery(query);
    this.updateState({ data: this.dto.address });
  };

  componentDidMount() {
    this.setId(this.props.id || 0);
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-lg-12">
            <GoogleMapWithSearchBar
              {...this.props}
              updateQuery={this.updateQuery}
              parseInput={this.parseInput}
            />
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-lg-12">
            {this.renderInput("latitude", "Latitude")}
            {this.renderInput("longitude", "Longitude")}
            {this.renderInput("street", "Street")}
            {this.renderInput("number", "Number")}
            {this.renderInput("city", "City")}
            {this.renderInput("zipcode", "Zip")}
            {this.renderInput("country", "Country")}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default GoogleMapWithAddress;
