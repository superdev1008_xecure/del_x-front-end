// Imports
import React, { Component } from "react";
import { connect } from "react-redux";

// Import Search Bar Components
import SearchBar from "material-ui-search-bar";

// Import React Scrit Libraray to load Google object
import Script from "react-load-script";
import { geolocated, geoPropTypes } from "react-geolocated";

const googleKey = process.env.REACT_APP_GOOGLE_API_KEY;

class MapGoogleSearchBar extends Component {
  state = {
    query: "",
    search: true,
    map: false,
    mount: false
  };

  elements = {
    map: null,
    autocomplete: null
  };

  gElements = {
    map: null,
    autocomplete: null,
    marker: null
  };

  geocoder;

  componentDidMount() {
    const { id, map, query, search } = this.props;

    if (map) this.elements.map = document.getElementById("map" + id);
    this.elements.autocomplete = document.getElementById("autocomplete" + id);

    this.setState({
      id,
      map: map !== undefined,
      mount: true,
      query,
      search: search === undefined
    });
  }

  handleScriptLoad = () => {
    const {
      polyline,
      markers,
      draggable,
      longitude,
      latitude,
      zoom
    } = this.props;
    const { mount } = this.state;
    if (!mount) {
      return setTimeout(this.handleScriptLoad, 100);
    }

    /*global google*/

    this.geocoder = new google.maps.Geocoder();

    if (this.state.map) {
      this.gElements.map = new google.maps.Map(this.elements.map, {
        zoom: zoom !== undefined ? zoom : 13
      });
    }

    // Declare Options For Autocomplete
    var options = {
      types: []
    }; // To disable any eslint 'google not defined' errors

    this.gElements.autocomplete = new google.maps.places.Autocomplete(
      this.elements.autocomplete,
      options
    );

    if (this.state.map)
      this.gElements.autocomplete.bindTo("bounds", this.gElements.map);
    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
    this.gElements.autocomplete.setFields([
      "address_components",
      "geometry",
      "place_id",
      "name"
    ]);

    // Fire Event when a suggested name is selected
    this.gElements.autocomplete.addListener(
      "place_changed",
      this.handlePlaceSelect
    );

    this.gElements.marker = new google.maps.Marker({
      map: this.gElements.map,
      //anchorPoint: new google.maps.Point(0, -29),
      draggable: draggable !== undefined ? draggable : true,
      animation: google.maps.Animation.DROP
    });

    this.gElements.marker.addListener("dragend", this.handleMarkerMove);

    if (polyline) {
      var path = google.maps.geometry.encoding.decodePath(polyline);
      if (path.length > 0) {
        new google.maps.Polyline({
          path: path,
          strokeColor: "#00008B",
          strokeOpacity: 1.0,
          strokeWeight: 4,
          zIndex: 3,
          geodesic: true,
          editable: false,
          map: this.gElements.map
        });

        //this.gElements.map.setCenter(path[0]);
        this.setMapBounds(path);
      }
    } else if (longitude && latitude) {
      this.moveMarkerToGPS(latitude, longitude);
    } else if (markers) {
      const list = this.renderMarkers(markers);
      if (list.length > 1) {
        this.setMapBounds(list);
      } else {
        this.gElements.map.setCenter(list[0]);
        this.gElements.map.zoom = 13;
      }
    } else {
      navigator.geolocation.getCurrentPosition(this.initPosition);
    }
  };

  setMapBounds = list => {
    const bounds = new google.maps.LatLngBounds();
    list.map(item => bounds.extend(item));
    this.gElements.map.fitBounds(bounds);
  };

  renderMarkers = path => {
    return path.map((item, key) => {
      const gps = new google.maps.LatLng(
        item.currentLatitude,
        item.currentLongitude
      );

      new google.maps.Marker({
        map: this.gElements.map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        position: gps
      });

      return gps;
    });
  };

  initPosition = address => {
    const { currentPositionChange } = this.props;
    this.moveMarkerToGPS(address.coords.latitude, address.coords.longitude);

    const { follow } = this.props;
    if (follow) {
      if (currentPositionChange)
        currentPositionChange(
          address.coords.latitude,
          address.coords.longitude
        );
      setTimeout(
        () => navigator.geolocation.getCurrentPosition(this.initPosition),
        10000
      );
    }
  };

  moveMarkerToGPS = (latitude, longitude) => {
    var latLng = new google.maps.LatLng(latitude, longitude);

    if (this.state.map && this.gElements.map && latLng) {
      this.gElements.map.setCenter(latLng);
      this.gElements.marker.setPosition(latLng);
      this.gElements.marker.setVisible(true);
    }
  };

  handlePlaceSelect = () => {
    const { parseInput, target } = this.props;
    // Extract City From Address Object
    this.gElements.marker.setVisible(false);
    var place = this.gElements.autocomplete.getPlace();

    if (!place.place_id) {
      window.alert("Please select an option from the dropdown list.");
      return;
    } else if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (this.state.map) {
      if (place.geometry.viewport) {
        this.gElements.map.fitBounds(place.geometry.viewport);
      } else {
        this.gElements.map.setCenter(place.geometry.location);
        this.gElements.map.setZoom(17); // Why 17? Because it looks good.
      }

      this.gElements.marker.setPosition(place.geometry.location);
      this.gElements.marker.setVisible(true);
    }
    // Check if address is valid
    if (parseInput)
      parseInput(
        target,
        place.place_id,
        place.address_components,
        place.geometry.location
      );

    this.handleChange();
  };

  handleMarkerMove = () => {
    this.geocodePosition(this.gElements.marker.getPosition());
  };

  geocodePosition = pos => {
    this.geocoder.geocode({ latLng: pos }, this.updateAddressByMarker);
  };

  updateAddressByMarker = responses => {
    const { parseInput, target } = this.props;

    if (responses && responses.length > 0) {
      let place = responses[0];

      if (parseInput)
        parseInput(
          target,
          place.place_id,
          place.address_components,
          place.geometry.location
        );

      this.setState({
        query: place.formatted_address || place.name
      });
    } else {
      alert("Cannot determine address at this location.");
    }
  };

  handleChange = () => {
    let s = this.gElements.autocomplete.gm_bindings_.manualSessions;
    let ret = "";
    do {
      if (Object.keys(s).length === 0) break;
      s = s[Object.keys(s)[0]];
    } while (s.formattedPrediction === undefined);

    ret = s.formattedPrediction;
    this.updateQuery(ret);
  };

  updateQuery = data => {
    const { updateQuery, target } = this.props;

    if (updateQuery) updateQuery(target, data);
    this.setState({ query: data });
  };

  render() {
    const { id, style, addresses, height, caption } = this.props;

    const { map, search, query } = this.state;
    const mapId = "map" + id;
    const autocompleteId = "autocomplete" + id;
    const address = addresses[id];

    if (address) {
      this.moveMarkerToGPS(address.latitude, address.longitude);
    }

    return (
      <div style={style}>
        {caption && (
          <div>
            <h3>{caption}</h3>
          </div>
        )}
        <SearchBar
          id={autocompleteId}
          placeholder="Search..."
          value={query}
          style={{
            margin: "0 auto",
            display: `${search ? "flex" : "none"}`
          }}
        />
        <div
          style={{
            height: height || 300,
            display: `${map ? "block" : "none"}`
          }}
        >
          <div id={mapId} />
        </div>
        <Script
          url={`https://maps.googleapis.com/maps/api/js?key=${googleKey}&libraries=places,geometry`}
          onLoad={this.handleScriptLoad}
        />
      </div>
    );
  }
}

MapGoogleSearchBar.propTypes = {
  ...MapGoogleSearchBar.propTypes,
  ...geoPropTypes
};

function mapStateToProps(state) {
  return {
    addresses: state.addresses
  };
}

export default connect(mapStateToProps)(
  geolocated({
    positionOptions: {
      enableHighAccuracy: false
    },
    userDecisionTimeout: 5000
  })(MapGoogleSearchBar)
);
