import React, { Component } from "react";
import auth from "../services/authService";
import { Redirect } from "react-router-dom";

export class Home extends Component {
  render() {
    const { user } = auth.getCurrentUser();
    if (user && user.transporter) return <Redirect to="/transporter/" />;
    else return <h1>Homepage</h1>;
  }
}

export default Home;
