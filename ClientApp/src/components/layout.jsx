import React, { Component } from "react";
import NavBarContainer from "./navBar";
import auth from "../services/authService";

export class Layout extends Component {
  state = {
    user: {}
  };
  displayName = Layout.name;

  componentDidMount() {
    const user = auth.getCurrentUser();
    this.setState({ user });
  }

  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <NavBarContainer user={user} />
        <main className="container">{this.props.children}</main>
      </React.Fragment>
    );
  }
}

export default Layout;
