import React, { Component } from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";

class MapContainer extends Component {
  state = {};
  render() {
    return (
      <Map
        google={this.props.google}
        zoom={8}
        style={mapStyles}
        initialCenter={{ lat: 47.444, lng: -122.176 }}
      >
        <Marker position={{ lat: 48.0, lng: -122.0 }} />
      </Map>
    );
  }
}

const mapStyles = {
  width: "50%",
  height: "50%"
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyDTG9wMeMgBjrhQ3AbtuMAAMpvk18AIBmI"
})(MapContainer);
