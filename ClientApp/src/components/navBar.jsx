import React from "react";
import { Link, NavLink } from "react-router-dom";

const NavBar = ({ user }) => {
  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <Link className="navbar-brand" to="/">
          TaxiParcel {process.env.NAME_POSTFIX}
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            {!user && (
              <React.Fragment>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/login">
                    Login
                  </NavLink>
                </li>
              </React.Fragment>
            )}
            {user && (
              <React.Fragment>
                {user.transporter && (
                  <React.Fragment>
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/transporter/home">
                        Home
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/transporter/work">
                        My work
                      </NavLink>
                    </li>
                  </React.Fragment>
                )}

                {!user.transporter && (
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/home">
                      Home
                    </NavLink>
                  </li>
                )}
                <React.Fragment>
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/registerParcel">
                      Register parcel
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/requestHistory">
                      Request history
                    </NavLink>
                  </li>
                </React.Fragment>

                <li className="nav-item">
                  <NavLink className="nav-link" to="/logout">
                    Logout
                  </NavLink>
                </li>
                <li
                  className="nav-item float-right"
                  style={{ position: "absolute", right: 10 }}
                >
                  <div className="nav-link">{user.email}</div>
                </li>
              </React.Fragment>
            )}
          </ul>
        </div>
      </nav>
    </React.Fragment>
  );
};

export default NavBar;
