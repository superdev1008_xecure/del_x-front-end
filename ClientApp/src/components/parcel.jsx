import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Checkbox from "./common/checkbox";
import FileUpload from "./common/fileUpload";
import DateTimePicker from "./common/dateTimePicker";
import { setParcelProperty } from "../redux/actions/parcelsActions";
import SelectExtended from "./common/selectExtended";
import InputExtended from "./common/inputExtended";

class Parcel extends Form {
  schema = {
    countryID: Joi.string()
      .required()
      .label("country"),
    parcelTypeID: Joi.string()
      .required()
      .label("parcel type"),
    currencyID: Joi.string()
      .required()
      .label("currency"),
    parcelValue: Joi.string()
      .required()
      .label("parcelPrice")
  };

  constructor(props) {
    super(props);
    this.state = this.inicializeState();
  }

  inicializeState() {
    return {
      errors: {},
      pictures: [],
      map: false
    };
  }

  setAddressFromStore = () => {
    const { id, address } = this.props;
    this.setData(address[id]);
  };

  onBlur = name => {
    const { onBlur } = this.props;
    if (onBlur) onBlur(name);
  };

  updateParcelPropertry = (id, name, value) => {
    const { setParcelProperty } = this.props;
    setParcelProperty(id, name, value);
  };

  handleCheckboxChange = (id, name, value) => {
    this.updateParcelPropertry(id, name, value);
  };

  handleSelectFile = (id, file) => {
    this.updateParcelPropertry(id, "parcelPicture", file);
  };

  handleSelectChange = (id, name, value) => {
    this.updateParcelPropertry(id, name, value);
  };

  handleInputChange = (id, name, value) => {
    this.updateParcelPropertry(id, name, value);
  };

  handleInputNumberChange = (id, name, value) => {
    this.updateParcelPropertry(id, name, +value);
  };

  handleDateChange = (id, name, value) => {
    this.updateParcelPropertry(id, name, value);
  };

  render() {
    this.setAddressFromStore();

    const { enums, id, readonly, parcels } = this.props;

    const insuranceId = `insurance-${id}`;
    const parcel = parcels.filter(i => i.id === id)[0] || {};

    const url = `${process.env.REACT_APP_API_URL}/api/Request/GetParcelImage/${id}`;

    return (
      <React.Fragment>
        <div className="row" style={{ paddingTop: 30 }}>
          {false && (
            <div className="col-md-4 col-sm-12 col-xs-12">
              <div className="row">
                {this.renderSelect(
                  "countryID",
                  "Country",
                  enums.countries.data,
                  "",
                  "",
                  "",
                  "",
                  readonly
                )}
              </div>
            </div>
          )}
          <div className="col-12 col-md-6">
            <div className="row">
              <SelectExtended
                id={id}
                name="parcelType"
                label="Parcel type"
                options={enums.parcelTypes.data}
                selectedValue={parcel.parcelType || ""}
                onChange={this.handleSelectChange}
                readonly={readonly}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="row">
              <SelectExtended
                id={id}
                name="parcelValueCurrency"
                label="Currency"
                options={enums.currencies.data}
                selectedValue={parcel.parcelValueCurrency || ""}
                onChange={this.handleSelectChange}
                readonly={readonly}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-md-6">
            <div className="row">
              <InputExtended
                id={id}
                type="number"
                name="parcelValue"
                label="Parcel price"
                value={parcel.parcelValue}
                onChange={this.handleInputNumberChange}
                readOnly={readonly}
                min="0"
                step="1"
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <div className="row">
                <label
                  className="col-form-label col-md-4"
                  htmlFor={insuranceId}
                >
                  Insurance
                </label>
                <div className="col-md-8">
                  <Checkbox
                    id={id}
                    name="insuranceRequested"
                    key={insuranceId}
                    disabled={readonly}
                    checked={parcel.checked}
                    onChange={this.handleCheckboxChange}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12 col-md-6">
            <div className="form-group">
              <div className="row">
                <label
                  className="col-form-label col-md-4"
                  htmlFor="loadingTime"
                >
                  Loading time
                </label>
                <div className="col-md-8">
                  <DateTimePicker
                    name={"requestedLoadingTime"}
                    label={""}
                    selectedDate={parcel.requestedLoadingTime}
                    onChange={this.handleDateChange}
                    disabled={readonly}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <div className="row">
                <label
                  className="col-form-label col-md-4"
                  htmlFor={`image-${id}`}
                >
                  Parcel photo
                </label>
                <div className="col-md-8">
                  <FileUpload
                    id={id}
                    url={url}
                    image={parcel.parcelPicture}
                    disabled={readonly}
                    filter=".jpg,.jpeg,.png"
                    handleSelectFile={this.handleSelectFile}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setParcelProperty: setParcelProperty
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    address: state.addresses,
    parcels: state.parcels
  };
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(Parcel);
