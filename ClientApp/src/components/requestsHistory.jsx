import React, { Component } from "react";
import { getTransportRequests } from "../services/apiService";
import DataListTable from "./tables/dataListTable";
import addressDTO from "../dto/addressDTO";

class RequestsHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }

  componentDidMount() {
    this.fetchRequests();
  }

  fetchRequests = async () => {
    const response = await getTransportRequests();
    if (response && response.status === 200) {
      this.setState({ data: this.prepareData("sender", response.data) });
    }
  };

  prepareData = (prefix, data) => {
    return data.map((item, key) => {
      return this.getAddress(item, prefix);
    });
  };

  getAddress = (data, prefix) => {
    const rtn = { ...addressDTO };
    rtn.id = data["id"] || "";
    rtn.created = new Date(data["created"]);
    rtn.addressID = data[prefix + "AddressID"] || "";
    rtn.street = data[prefix + "Street"] || "";
    rtn.city = data[prefix + "City"] || "";
    rtn.houseNr = data[prefix + "HouseNr"] || "";
    rtn.zip = data[prefix + "Zip"] || "";
    rtn.latitude = data[prefix + "Latitude"] || "";
    rtn.longitude = data[prefix + "Longitude"] || "";
    rtn.phone = data[prefix + "Phone"] || "";
    rtn.email = data[prefix + "Email"] || "";
    rtn.country = data[prefix + "Country"] || "";
    rtn.deliveryStatus = +(data["status"] || 0);
    return rtn;
  };

  render() {
    const { data } = this.state;
    return (
      <React.Fragment>
        <DataListTable data={data} />
      </React.Fragment>
    );
  }
}

export default RequestsHistory;
