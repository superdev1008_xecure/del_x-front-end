import React, { Component } from "react";
import Table from "../common/table";
import { paginate } from "../../utils/paginate";
import _ from "loadsh";
import { Link } from "react-router-dom";
import { getRequestInfo } from "../../utils/requestStatus";

class DataListTable extends Component {
  state = {
    showAddress: false,
    //data: [],
    currentPage: 1,
    pageSize: 10000,
    searchQuery: "",
    genres: [],
    sortColumn: { path: "created", order: "asc" }
  };

  columns = [
    {
      path: "created",
      label: "Created",
      content: item => (
        <Link to={`/registerParcel/${item.id}`}>
          {item.created.toLocaleString()}
        </Link>
      )
    },
    { path: "address", label: "Address", hidden: "md" },
    { path: "email", label: "E-mail", hidden: "md" },
    { path: "status", label: "Status" }
  ];

  onSelect = record => {
    const { onSelect, name } = this.props;
    if (onSelect) onSelect(name, record);
  };

  onBlur = () => {};

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  getPagedData = () => {
    const { pageSize, currentPage, searchQuery, sortColumn } = this.state;

    const allData = this.props.data;

    let filtered = allData;

    if (searchQuery) {
      filtered = allData.filter(m => {
        let match = false;
        for (const key of Object.keys(m)) {
          if (
            (m[key] || "")
              .toString()
              .toLowerCase()
              .indexOf(searchQuery.toLowerCase()) > -1
          ) {
            match = true;
            break;
          }
        }
        return match;
      });
    }

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    const data = paginate(sorted, currentPage, pageSize).map(item =>
      getRequestInfo(item)
    );
    return { totalCount: filtered.length, data };
  };

  render() {
    const { sortColumn } = this.state;
    const { data } = this.getPagedData();

    return (
      <Table
        columns={this.columns}
        data={data}
        onSort={this.handleSort}
        sortColumn={sortColumn}
      />
    );
  }
}

export default DataListTable;
