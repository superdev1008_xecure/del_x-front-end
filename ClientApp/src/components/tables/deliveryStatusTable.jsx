import React, { Component } from "react";
import Table from "../common/table";
import { paginate } from "../../utils/paginate";
import { getDeliveryInfo } from "../../utils/deliveryStatus";
import _ from "loadsh";

class DeliveryStatusTable extends Component {
  state = {
    showAddress: false,
    //data: [],
    currentPage: 1,
    pageSize: 10000,
    searchQuery: "",
    genres: [],
    sortColumn: { path: "number", order: "asc" }
  };

  columns = [
    { path: "number", label: "Parcel #" },
    { path: "address", label: "Address", hidden: "sm" },
    { path: "email", label: "E-mail", hidden: "sm" },
    { path: "status", label: "Status" },
    { path:"secretDeliveryCode", label:" Delivery code"}
  ];

  onSelect = record => {
    const { onSelect, name } = this.props;
    if (onSelect) onSelect(name, record);
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  getPagedData = () => {
    const { pageSize, currentPage, searchQuery, sortColumn } = this.state;
    const { data: rows } = this.props;

    const allData = rows;

    let filtered = allData;

    if (searchQuery) {
      filtered = allData.filter(m => {
        let match = false;
        for (const key of Object.keys(m)) {
          if (
            (m[key] || "")
              .toString()
              .toLowerCase()
              .indexOf(searchQuery.toLowerCase()) > -1
          ) {
            match = true;
            break;
          }
        }
        return match;
      });
    }

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    const data = paginate(sorted, currentPage, pageSize).map(item =>
      getDeliveryInfo(item)
    );
    return { totalCount: filtered.length, data };
  };

  render() {
    const { sortColumn } = this.state;
    const { data } = this.getPagedData();
//console.log(data);
    return (
      <Table
        columns={this.columns}
        data={data}
        onSort={this.handleSort}
        sortColumn={sortColumn}
      />
    );
  }
}

export default DeliveryStatusTable;
