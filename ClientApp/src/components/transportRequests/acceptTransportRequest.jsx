import React from "react";
import { getRequest } from "../../services/apiService";
import Form from "../common/form";
import { transformFromRequest } from "../../dto/transportRequest";

class AcceptTransportRequest extends Form {
  schema = {};
  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState = () => {
    return {
      data: {},
      errors: {}
    };
  };

  componentDidMount() {
    this.initData();
  }

  initData = async () => {
    const { match } = this.props;
    let data = transformFromRequest();

    const response = await getRequest(match.params.id);
    if (response && response.status === 200) {
      data = transformFromRequest(response.data);
    }

    this.setState({ data });
  };

  acceptTransportRequest = () => {
    const { data } = this.state;
    window.location.href = data.paymentUrl;
  };

  render() {
    return (
      <React.Fragment>
        <h1>Register transport request</h1>
        <hr />

        <form>
          {this.renderButton(
            "Accept transport request",
            this.acceptTransportRequest,
            "btn-primary btn-sm float-right",
            { marginBottom: 20 }
          )}
        </form>
      </React.Fragment>
    );
  }
}

export default AcceptTransportRequest;
