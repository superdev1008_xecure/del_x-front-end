import React, { Component } from "react";
import GoogleMapWithSearchBar from "../google/googleMapWithSearchBar";
import DeliveryStatusTable from "../tables/deliveryStatusTable";

class DeliveryStatus extends Component {
  state = { data: [], selected: null };

  componentDidMount() {
    const { data } = this.props;
    this.setState({ data, selected: data[0].id });
  }

  handleClick = id => {
    this.setState({ selected: id });
  };

  render() {
    const { data } = this.state;

    const markers = [...data];

    return (
      <React.Fragment>
        <div className="row" style={{ marginTop: 20 }}>
          <div className="col-12">
            <h2>Delivery status</h2>
            <hr />
          </div>
          <div className="col-12 col-md-6">
            <DeliveryStatusTable data={data} handleClick={this.handleClick} />
          </div>
          <div className="col-12 col-md-6">
            <GoogleMapWithSearchBar
              search={false}
              map
              draggable={false}
              markers={markers}
              caption="Current location of each package"
              id="currentLocationPackages"
              target="currentLocationPackages"
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default DeliveryStatus;
