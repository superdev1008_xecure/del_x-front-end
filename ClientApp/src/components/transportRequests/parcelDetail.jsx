import React, { Component } from "react";
import { connect } from "react-redux";
import { getParcelDetail } from "../../services/apiService";
import { parseParcelDetail } from "../../dto/transportRequest";
import { bindActionCreators } from "redux";
import ParcelRequestForm from "../forms/parcelRequestForm";
import GoogleMapWithSearchBar from "../google/googleMapWithSearchBar";
import Loading from "../common/loading";
import TransporterInformation from "./transporterInformation";

class ParcelDetail extends Component {
  state = { loading: true, data: {} };

  componentDidMount() {
    this.initData();
  }

  initData = async () => {
    const { match } = this.props;
    const { request, parcel } = match.params;

    const response = await getParcelDetail(request, parcel);

    if (response && response.status === 200) {
      const data = parseParcelDetail(response.data);

      this.setState({ loading: false, data });
    }
  };

  render() {
    const { loading, data } = this.state;
    const { enums } = this.props;

    return loading ? (
      <Loading />
    ) : (
      <React.Fragment>
        <TransporterInformation
          data={data}
          caption="Parcel detail"
          secretCode
        ></TransporterInformation>
        {false && (
          <GoogleMapWithSearchBar
            search={false}
            map
            height="80vh"
            draggable={false}
            markers={[data]}
            caption="Current location of package"
            id="currentLocationPackage"
            target="currentLocationPackage"
          />
        )}
        {false && (
          <ParcelRequestForm
            label="Parcel detail"
            key={data.id}
            index={1}
            id={data.id}
            enums={enums}
            readonly={true}
            defaultValues={data}
          />
        )}
      </React.Fragment>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

function mapStateToProps(state) {
  return {
    enums: state.enums
  };
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(ParcelDetail);
