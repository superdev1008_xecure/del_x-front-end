import React from "react";
import ParcelRequestForm from "./../forms/parcelRequestForm";
import { connect } from "react-redux";
import Form from "../common/form";
import { bindActionCreators } from "redux";
import { setAddress, clearAddress } from "../../redux/actions/addressActions";
import { setAddresses } from "../../redux/actions/addressBookActions";
import { addParcel, removeParcel } from "./../../redux/actions/parcelsActions";
import { transformFromRequest } from "../../dto/transportRequest";
import service from "../../services/apiService";
import { toast } from "react-toastify";
import TransportRequest from "./../../dto/transportRequest";
import Joi from "joi-browser";
import requestStatus from "../../utils/transportRequestStatus";
import WholeRoute from "./wholeRoute";
import {
  setCurrentRequest,
  clearCurrentRequest
} from "../../redux/actions/currentRequestActions";
import TransporterInformation from "./transporterInformation";
import DeliveryStatus from "./deliveryStatus";
import Reloader from "../common/reload";
import Loading from "../common/loading";

class RegisterTransportRequest extends Form {
  data = {
    totalDistance: 0,
    totalPrice: 0,
    licencePlate: "",
    lastName: "",
    firstName: "",
    mobilePhoneNr: ""
  };

  status = requestStatus.Unset;

  schema = {
    countryID: Joi.string()
      .allow("")
      .label("Country"),
    parcelTypeID: Joi.string().label("Parcel Type"),
    currencyID: Joi.string().label("Currency"),
    parcelValueID: Joi.string()
      .allow("")
      .label("Parcel Value"),
    parcelValue: Joi.string()
      .allow("")
      .label("ParcelValue"),
    licencePlate: Joi.string()
      .allow("")
      .label("LicencePlate"),
    lastName: Joi.string()
      .allow("")
      .label("LastName"),
    firstName: Joi.string()
      .allow("")
      .label("FirstName"),
    mobilePhoneNr: Joi.string()
      .allow("")
      .label("Phone Number")
  };

  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState = () => {
    return {
      errors: {},
      pictures: [],
      loading: false,
      map: false
    };
  };

  componentDidMount() {
    const { parcels, match, clearCurrentRequest } = this.props;

    if (match.params.id) {
      this.getData(match.params.id);
    } else {
      clearCurrentRequest();
    }

    if (parcels.length === 0) {
      this.addParcel();
    }
  }

  addParcel = () => {
    const { addParcel } = this.props;
    addParcel("parcel_" + this.guidGenerator());
  };

  removeParcel = id => {
    const { removeParcel } = this.props;
    removeParcel(id);
  };

  handleAddressChange = (name, address) => {
    const { setAddress } = this.props;
    setAddress(name, address);
  };

  handleSelectAddress = (name, address) => {
    this.closeModal();
    this.handleAddressChange(name, address);
  };

  handleSelectFile = (index, file) => {
    const { pictures } = this.state;
    pictures[index] = file;
    this.setState({ pictures });
  };

  handleSave = async name => {
    const { showAddress } = this.state;
    const { setAddresses, addresses } = this.props;
    const response = await service.addAddress(addresses[name]);
    this.setState({ data: response.data, showAddress: !showAddress }, () => {
      setAddresses(this.state.data);
      toast.success("Address saved", { autoClose: 5000 });
    });
  };

  acceptTransportRequest = async () => {
    const response = await service.acceptTransportRequestByClient(this.data.id);
    if (response && response.status === 200) {
      toast.success("Confirmed", { autoClose: 5000 });
      window.location.href = response.data.paymentUrl;
    } else {
      toast.error("Error", response.status);
    }
  };

  loadConfirmedByOwner = async () => {
    const response = await service.loadConfirmedByOwner(this.data.id);
    if (response && response.status === 200) {
      toast.success("Confirmed", { autoClose: 5000 });
      //history.push("/registerParcel/" + this.data.id);
      this.getData(this.data.id);
    } else {
      toast.error("Error", response.status);
    }
  };

  sendTransportRequest = async () => {
    const { addresses, parcels, history } = this.props;
    //console.log("SEND_TRANSPORT_REQUEST");
    const request = new TransportRequest();
    const sender = addresses["sender"] || {};
    //console.log("SENDER", sender);
    request.updateSender(sender);
    for (let index in parcels) {
      const parcel = parcels[index];
      if (parcel) {
        const address = addresses[parcel.id] || {};
        request.addItem(address, parcel);
      }
    }

    console.log("REQUEST_DATA", request.getData());
    //return;
    const response = await service.sendTransportRequest(request.getData());

    if (response && response.status === 200) {
      toast.success("Saved", { autoClose: 5000 });
      history.push("/registerParcel/" + response.data.id);
      this.getData(response.data.id, false);
    } else {
      toast.error("Error", response.status);
    }
  };

  onBlur = () => {};

  getData = async (id, showLoading = true) => {
    const { setCurrentRequest, currentRequest } = this.props;

    if (showLoading) this.setState({ loading: true });

    const response = await service.getRequest(id);
    if (response && response.status === 200) {
      //console.log("RESPONSE", response.data);
      this.data = transformFromRequest(response.data);
      //console.log("DATA", this.data);
      if (this.data.status > requestStatus.Unset) {
        setTimeout(() => this.reload(id), 60000);
        //console.log("TICK");
      }
      setCurrentRequest(this.data);

      if (showLoading) this.setState({ loading: false });
    }
  };

  reload = async id => {
    this.getData(id, false);
  };

  render() {
    const {
      parcels,
      enums,
      currentRequest,
      match,
      clearCurrentRequest
    } = this.props;

    if (currentRequest.id && !match.params.id) {
      console.log("CLEAR");
      clearCurrentRequest();
    }

    let index = 1;
    const items =
      currentRequest.status > requestStatus.Unset && currentRequest.items
        ? currentRequest.items
        : parcels;

    const { loading } = this.state;
    return loading ? (
      <Loading />
    ) : (
      <React.Fragment>
        <h1>Register transport request</h1>
        <hr />
        <form onSubmit={this.handleSubmit}>
          <React.Fragment>
            <ParcelRequestForm
              label="Sender"
              key="sender"
              id="sender"
              onChange={this.handleAddressChange}
              onSave={this.handleSave}
              readonly={currentRequest.status > requestStatus.Unset}
              defaultValues={currentRequest && currentRequest.sender}
            />
          </React.Fragment>
          {items.map((parcel, i) => {
            const id = parcel.id;
            return (
              <React.Fragment key={id}>
                <hr />
                <ParcelRequestForm
                  key={id}
                  index={index++}
                  id={id}
                  enums={enums}
                  readonly={currentRequest.status > requestStatus.Unset}
                  onRemove={() => this.removeParcel(id)}
                  onChange={this.handleAddressChange}
                  onSave={this.handleSave}
                  defaultValues={parcel}
                />
              </React.Fragment>
            );
          })}
          {currentRequest.id === undefined && (
            <React.Fragment>
              <hr />
              {this.renderButton(
                "Send transport request",
                this.sendTransportRequest,
                "btn-primary btn-sm float-right"
              )}
              {this.renderButton(
                "Add parcel",
                this.addParcel,
                "btn-primary btn-sm"
              )}
            </React.Fragment>
          )}

          {currentRequest.status > requestStatus.Unset && (
            <WholeRoute
              data={currentRequest}
              accept={this.acceptTransportRequest}
            />
          )}
          {currentRequest.status > requestStatus.Paid && (
            <TransporterInformation
              data={currentRequest}
              action={this.loadConfirmedByOwner}
            />
          )}
          {currentRequest.status >= requestStatus.HandoverToTransporterDone && (
            <DeliveryStatus data={currentRequest.items}></DeliveryStatus>
          )}
        </form>
      </React.Fragment>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setAddresses: setAddresses,

      setAddress: setAddress,
      clearAddress: clearAddress,

      addParcel: addParcel,
      removeParcel: removeParcel,

      setCurrentRequest: setCurrentRequest,
      clearCurrentRequest: clearCurrentRequest
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    addresses: state.addresses,
    parcels: state.parcels,
    enums: state.enums,
    currentRequest: state.currentRequest
  };
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(RegisterTransportRequest);
