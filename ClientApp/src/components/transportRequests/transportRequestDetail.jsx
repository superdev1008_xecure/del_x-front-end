import React from "react";
import ParcelRequestForm from "../forms/parcelRequestForm";
import { DualRing } from "react-loading-io";

const TransportRequestDetail = ({ data, loading }) => {
  let index = 1;

  return loading ? (
    <React.Fragment>
      <div className="text-center">
        <DualRing size={64} color="#4CA7FD" speed={1.50} width={7} height={15} />{" "}
        <h2>Loading data. Please wait&hellip;</h2>
      </div>
    </React.Fragment>
  ) : (
    <React.Fragment>
      <ParcelRequestForm
        label="Sender"
        key="sender"
        id="sender"
        readonly
        defaultValues={data.sender}
      />
      {data.items.map(item => {
        const { id } = item;
        return (
          <React.Fragment key={id}>
            <hr />
            <ParcelRequestForm
              key={id}
              index={index++}
              id={id}
              defaultValues={item}
              readonly
            />
          </React.Fragment>
        );
      })}
    </React.Fragment>
  );
};

export default TransportRequestDetail;
