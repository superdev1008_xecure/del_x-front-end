import React, { Component } from "react";
import { getRequest } from "../../services/apiService";
import { transformFromRequest } from "../../dto/transportRequest";
import TransportRequestDetail from "./transportRequestDetail";

class TransportRequestStatus extends Component {
  state = {};
  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState = () => {
    return {
      loading: true,
      data: {},
      errors: {}
    };
  };

  data = {};

  componentDidMount() {
    this.initData();
  }

  initData = async () => {
    const { match } = this.props;
    let data = transformFromRequest();

    const response = await getRequest(match.params.id);

    if (response && response.status === 200) {
      data = transformFromRequest(response.data);
    }

    this.setState({ loading: false, data });
  };

  render() {
    const { loading, data } = this.state;

    return (
      <React.Fragment>
        <h1>Transport request detail</h1>
        <hr />

        <form>
          <TransportRequestDetail
            data={data}
            loading={loading}
          ></TransportRequestDetail>
        </form>
      </React.Fragment>
    );
  }
}

export default TransportRequestStatus;
