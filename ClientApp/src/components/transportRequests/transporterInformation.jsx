import React from "react";
import Form from "../common/form";
import GoogleMapWithSearchBar from "../google/googleMapWithSearchBar";
import requestStatus from "../../utils/transportRequestStatus";
import InputExtended from "../common/inputExtended";
class TransporterInfromation extends Form {
  state = {};

  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState = () => {
    return {
      errors: {}
    };
  };

  onBlur = () => {};

  render() {
    const {
      secretCode,
      data,
      action: loadConfirmedByOwner,
      caption
    } = this.props;
    this.setData(data.transporter);

    //console.log(data);

    return (
      <React.Fragment>
        <div className="row" style={{ marginTop: 20 }}>
          <div className="col-12">
            <h2>{caption || "Transporter information"}</h2>
            <hr />
          </div>
          <div className="col-12 col-md-6">
            {secretCode && (
              <InputExtended
                id=""
                label="Delivery code"
                name="secretCode"
                readOnly
                value={data.secretDeliveryCode}
              />
            )}
            {this.renderInput(
              "licencePlate",
              "License plate",
              "text",
              "",
              "",
              "",
              false,
              "horizontal"
            )}
            {this.renderInput(
              "firstName",
              "First name",
              "text",
              "",
              "",
              "",
              false,
              "horizontal"
            )}
            {this.renderInput(
              "lastName",
              "Last name",
              "text",
              "",
              "",
              "",
              false,
              "horizontal"
            )}
            {this.renderInput(
              "mobilePhoneNr",
              "Phone number",
              "text",
              "",
              "",
              "",
              false,
              "horizontal"
            )}
          </div>
          <div className="col-12 col-md-6">
            <GoogleMapWithSearchBar
              search={false}
              map
              draggable={false}
              id="currentPosition"
              target="currentPosition"
              caption="Current position"
              latitude={this.data.latitude}
              longitude={this.data.longitude}
            />
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-12">
            {data.status === requestStatus.TransporterAssigned &&
              !data.loadAcceptedByOwner &&
              data.transporter &&
              this.renderButton(
                "Confirm Packages Handover",
                loadConfirmedByOwner,
                "btn-primary btn-sm float-right"
              )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default TransporterInfromation;
