import React from "react";
import GoogleMapWithSearchBar from "../google/googleMapWithSearchBar";
import requestStatus from "../../utils/transportRequestStatus";
import Form from "../common/form";
import Joi from "joi-browser";
import authService from "../../services/authService";

class WholeRoute extends Form {
  state = {};

  constructor() {
    super();
    this.state = this.inicializeState();
  }

  inicializeState = () => {
    return {
      errors: {}
    };
  };

  schema = {
    totalDistance: Joi.string()
      .allow("")
      .label("Total Distance(km)"),
    totalPrice: Joi.string()
      .allow("")
      .label("Total Price")
  };

  render() {
    const {
      data,
      accept: acceptTransportRequest,
      decline: declineTransportRequest,
      transporter,
      caption,
      mapCaption
    } = this.props;
    this.setData(data);
    const user = authService.getCurrentUser();
    const mapId = `wholeRoute-${data.id}`;
    return (
      <React.Fragment>
        <div className="row" style={{ marginTop: 20 }}>
          <div className="col-12">
            <a name="status_2">
              <h2>{caption || "Whole Route"}</h2>
            </a>
            <hr />
          </div>
          <div className="col-12 col-md-6">
            {this.renderInput(
              "totalDistance",
              "Total distance",
              "text",
              "",
              "",
              "",
              false,
              "horizontal"
            )}
            {this.renderInput(
              "totalPrice",
              "Total price",
              "text",
              "",
              "",
              "",
              false,
              "horizontal"
            )}
          </div>
          <div className="col-12 col-md-6">
            {data.routePolyline && (
              <GoogleMapWithSearchBar
                search={false}
                map
                zoom={10}
                caption={mapCaption || "Actual route"}
                id={mapId}
                target={mapId}
                polyline={data.routePolyline}
              />
            )}
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-12">
            {transporter &&
              user.transporter &&
              this.renderButton(
                "Decline Order",
                declineTransportRequest,
                "btn-danger btn-sm float-left"
              )}
            {(data.status === requestStatus.Sent ||
              (transporter && user.transporter)) &&
              this.renderButton(
                "Confirm Order",
                acceptTransportRequest,
                "btn-primary btn-sm float-right"
              )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default WholeRoute;
