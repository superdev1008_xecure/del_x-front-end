import React from "react";
import WholeRoute from "../transportRequests/wholeRoute";
import Form from "../common/form";
import { transformFromRequest } from "../../dto/transportRequest";
import service from "../../services/apiService";
import { toast } from "react-toastify";
import Loading from "../common/loading";

class TransporterAcceptRequest extends Form {
  state = {
    loading: true
  };

  componentDidMount() {
    const { match } = this.props;
    this.getData(match.params.id);
  }
  data = {};

  getData = async id => {
    const response = await service.getRequest(id);
    if (response && response.status === 200) {
      this.data = transformFromRequest(response.data);
      this.setState({ loading: false });
    }
  };

  acceptTransportRequest = async () => {
    const { history } = this.props;

    const response = await service.acceptTransportRequest(this.data.id);
    if (response && response.status === 200) {
      toast.success("Confirmed", { autoClose: 5000 });
      history.push("/transporter/detail/" + this.data.id);
    } else {
      toast.error("Error", response.status);
    }
  };

  render() {
    const { loading } = this.state;
    return loading ? (
      <Loading />
    ) : (
      <React.Fragment>
        <WholeRoute
          transporter
          data={this.data}
          action={this.acceptTransportRequest}
        />
      </React.Fragment>
    );
  }
}

export default TransporterAcceptRequest;
