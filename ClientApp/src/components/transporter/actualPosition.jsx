import React, { Component } from "react";
import GoogleMapWithSearchBar from "../google/googleMapWithSearchBar";
import service from "../../services/apiService";

class ActualPosition extends Component {
  state = {};

  handleCurrentPositionChange = async (lat, lon) => {
    console.log("GPS", lat + ", " + lon);

    const response = service.transporterUpdateLastLocation(lat, lon);
    if (response && response.status === 200) {
      console.log(response);
    }
  };

  render() {
    const id = "driverPosition";
    const query = "";
    return (
      <GoogleMapWithSearchBar
        search={false}
        follow
        height={500}
        id={id}
        target={id}
        query={query}
        currentPositionChange={this.handleCurrentPositionChange}
      />
    );
  }
}

export default ActualPosition;
