import React from "react";
import Form from "../common/form";
import TransportRequestSignalR from "./transportRequestSignalR";
import ActualPosition from "./actualPosition";

class TransporterHome extends Form {
  state = {};

  render() {
    return (
      <React.Fragment>
        <h1>Transporter home</h1>
        <ActualPosition></ActualPosition>
        <TransportRequestSignalR />
      </React.Fragment>
    );
  }
}

export default TransporterHome;
