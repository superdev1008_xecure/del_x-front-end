import React from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import { transformFromRequest } from "../../dto/transportRequest";
import service from "../../services/apiService";
import transportRequestStatus from "../../utils/transportRequestStatus";
import { toast } from "react-toastify";
import Loading from "../common/loading";

class TransporterTransportRequestDetail extends Form {
  schema = {
    parcelCode: Joi.string()
      //.allow("")
      .label("Parcel")
  };
  data = {};

  state = {
    loading: true,

    errors: {}
  };

  componentDidMount() {
    const { match } = this.props;
    this.getData(match.params.id);
  }

  openNavigation = url => {
    window.location.href =
      url ||
      `https://www.google.com/maps/dir/?api=1&destination=${this.data.sender.latitude},${this.data.sender.longitude}&travelmode=driving`;
  };

  packagesLoaded = async () => {
    this.setState({ loading: true });
    const response = await service.transporterPackagesLoaded(this.data.id);
    if (response && response.status === 200) {
      toast.success("Packages loaded", { autoClose: 5000 });
      this.getData(this.data.id);
    } else {
      toast.error("Error", response.status);
    }
  };

  packageDelivered = async () => {
    const response = await service.transporterPackageDelivered(
      this.data.parcelCode
    );
    if (response && response.status === 200) {
      const { result } = response.data;
      if (result) {
        toast.success("Parcel added", { autoClose: 5000 });
        this.data.parcelCode = "";
      } else toast.error("Wrong delivery code", { autoClose: 5000 });

      const { loading } = this.state;
      this.setState({ loading });
    } else {
      toast.error("Error", response.status);
    }
  };

  getData = async id => {
    const response = await service.getRequest(id);
    if (response && response.status === 200) {
      this.data = transformFromRequest(response.data);
      this.setState({ loading: false }, () => {});
    }
  };

  render() {
    const { loading } = this.state;
    return loading ? (
      <Loading />
    ) : (
      <React.Fragment>
        <h2>Transport request detail</h2>
        {!this.data.loadAcceptedByTransporter && (
          <React.Fragment>
            {this.renderButton(
              "Open navigation to sender",
              this.openNavigation,
              "btn btn-sm btn-primary"
            )}
            &nbsp;
            {this.renderButton(
              "Parcels has been loaded",
              this.packagesLoaded,
              "btn btn-sm btn-primary"
            )}
          </React.Fragment>
        )}
        {this.data.loadAcceptedByTransporter &&
          this.data.status >=
            transportRequestStatus.HandoverToTransporterDone &&
          this.data.status < transportRequestStatus.Delivered && (
            <React.Fragment>
              <div className="row">
                <div className="col-12 col-md-6">
                  {this.renderInput(
                    "parcelCode",
                    "Delivery code from reciever",
                    "text"
                  )}
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-6">
                  {this.renderButton(
                    "Parcel delivered",
                    this.packageDelivered,
                    "btn btn-primary"
                  )}
                  &nbsp;
                  {this.renderButton(
                    "Open route",
                    () => this.openNavigation(this.data.navigationRouteLink),
                    "btn btn-primary"
                  )}
                </div>
              </div>
            </React.Fragment>
          )}
      </React.Fragment>
    );
  }
}

export default TransporterTransportRequestDetail;
