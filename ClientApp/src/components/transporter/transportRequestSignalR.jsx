import React, { Component } from "react";
import { toast } from "react-toastify";
import { HubConnectionBuilder } from "@aspnet/signalr";
import { NavLink } from "react-router-dom";
import { withRouter } from "react-router-dom";
import service from "../../services/apiService";
import WholeRoute from "../transportRequests/wholeRoute";
import Loading from "../common/loading";

class TransportRequestSignalR extends Component {
  state = {
    loading: true,
    data: [],
    hubConnection: null
  };

  componentDidMount() {
    const hubConnection = new HubConnectionBuilder()
      .withUrl("https://del-xtestapi.azurewebsites.net/TransportJobHub")
      .build();

    this.loadData();

    this.setState({ hubConnection }, this.updateState);
  }

  loadData = async () => {
    const response = await service.getAllUnacceptedByTransporter();
    if (response && response.status === 200) {
      this.setState({ data: response.data, loading: false });
    } else {
      toast.error("Error", response.status);
    }
  };

  updateState = () => {
    const { hubConnection } = this.state;
    //const { history } = this.props;

    hubConnection
      .start()
      .then(() => console.log("Connection started!"))
      .catch(err => {
        toast.error("Connection not started!");
        console.log("Error while establishing connection :(");
      });

    hubConnection.on("NewTransportRequest", id => {
      toast.success("New transport request!");
      //history.push(`/transporter/acceptRequest/${id}`);
      this.addTransportRequest(id);
    });
  };

  addTransportRequest = async id => {
    const { data } = this.state;
    const response = await service.getRequest(id);
    if (response && response.status === 200) {
      data.push(response.data);
      this.setState({ data });
    }
  };

  acceptTransportRequest = async id => {
    const { history } = this.props;

    const response = await service.acceptTransportRequest(id);
    if (response && response.status === 200) {
      toast.success("Confirmed", { autoClose: 5000 });
      history.push("/transporter/detail/" + id);
    } else {
      toast.error("Error", response.status);
    }
  };

  declineTransportRequest = async id => {
    const response = await service.declineTransportRequest(id);
    if (response && response.status === 200) {
      this.setState({ data: this.state.data.filter(i => i.id !== id) });
    } else {
      toast.error("Error", response.status);
    }
  };

  render() {
    const { data, loading } = this.state;
    let count = 0;
    return loading ? (
      <Loading />
    ) : data.length > 0 ? (
      <React.Fragment>
        {data.map(item => {
          count++;
          const key = `route-${item.id}`;

          return (
            <div key={key} className="row">
              <div className="col-12">
                <WholeRoute
                  caption={`Requested route #${count}`}
                  mapCaption=""
                  transporter
                  data={item}
                  accept={() => this.acceptTransportRequest(item.id)}
                  decline={() => this.declineTransportRequest(item.id)}
                />
              </div>
            </div>
          );
        })}
      </React.Fragment>
    ) : (
      <h4>Wait for transport requests!</h4>
    );
  }
}

export default withRouter(TransportRequestSignalR);
