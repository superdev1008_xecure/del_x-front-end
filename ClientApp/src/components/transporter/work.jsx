import React, { Component } from "react";
import TransporterWorkTable from "./../tables/transporterWorkTable";
import { getAllOwnUndeliveredTransportRequests } from "./../../services/apiService";
import addressDTO from "../../dto/addressDTO";

class TransporterWork extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }

  componentDidMount() {
    this.fetchRequests();
  }

  fetchRequests = async () => {
    const response = await getAllOwnUndeliveredTransportRequests();
    if (response && response.status === 200) {
      this.setState({ data: this.prepareData("sender", response.data) });
    }
  };

  prepareData = (prefix, data) => {
    return data.map((item, key) => {
      return this.getAddress(item, prefix);
    });
  };

  getAddress = (data, prefix) => {
    const rtn = { ...addressDTO };
    rtn.id = data["id"] || "";
    rtn.created = new Date(data["created"]);
    rtn.addressID = data[prefix + "AddressID"] || "";
    rtn.street = data[prefix + "Street"] || "";
    rtn.city = data[prefix + "City"] || "";
    rtn.houseNr = data[prefix + "HouseNr"] || "";
    rtn.zip = data[prefix + "Zip"] || "";
    rtn.latitude = data[prefix + "Latitude"] || "";
    rtn.longitude = data[prefix + "Longitude"] || "";
    rtn.phone = data[prefix + "Phone"] || "";
    rtn.email = data[prefix + "Email"] || "";
    rtn.country = data[prefix + "Country"] || "";
    rtn.deliveryStatus = +(data["status"] || 0);
    return rtn;
  };

  render() {
    const { data } = this.state;
    return (
      <React.Fragment>
        <h1>My work</h1>
        <TransporterWorkTable data={data} />
      </React.Fragment>
    );
  }
}

export default TransporterWork;
