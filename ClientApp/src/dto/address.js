class Address {
  address = {
    id: null,
    search: "",
    street: "",
    number: "",
    city: "",
    zipcode: "",
    country: "",
    latitude: "",
    longitude: "",
    addressID: "",
    placeID: ""
  };

  address_component = {
    // indicates a precise street address.
    street_address: null,

    // indicates a named route (such as "US 101").
    route: null,

    // indicates a major intersection, usually of two major roads.
    intersection: null,

    // indicates a political entity. Usually, this type indicates a polygon of some civil administration.
    political: null,

    // indicates the national political entity, and is typically the highest order type returned by the Geocoder.
    country: null,

    // indicates a first-order civil entity below the country level. Within the United States, these administrative levels are states. Not all nations exhibit these administrative levels. In most cases, administrative_area_level_1 short names will closely match ISO 3166-2 subdivisions and other widely circulated lists; however this is not guaranteed as our geocoding results are based on a variety of signals and location data.
    administrative_area_level_1: null,

    // indicates a second-order civil entity below the country level. Within the United States, these administrative levels are counties. Not all nations exhibit these administrative levels.
    administrative_area_level_2: null,

    // indicates a third-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
    administrative_area_level_3: null,

    // indicates a fourth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
    administrative_area_level_4: null,

    // indicates a fifth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
    administrative_area_level_5: null,

    // indicates a commonly-used alternative name for the entity.
    colloquial_area: null,

    // indicates an incorporated city or town political entity.
    locality: null,

    //  indicates a first-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
    sublocality: null,

    // indicates a named neighborhood
    neighborhood: null,

    // indicates a named location, usually a building or collection of buildings with a common name
    premise: null,

    // indicates a first-order entity below a named location, usually a singular building within a collection of buildings with a common name
    subpremise: null,

    // indicates a postal code as used to address postal mail within the country.
    postal_code: null,

    // indicates a prominent natural feature.
    natural_feature: null,

    // indicates an airport.
    airport: null,

    // indicates a named park.
    park: null,

    // indicates a named point of interest. Typically, these "POI"s are prominent local entities that don't easily fit in another category, such as "Empire State Building" or "Eiffel Tower".
    point_of_interest: null,

    // ADDITION

    // indicates the floor of a building address.
    floor: null,

    // typically indicates a place that has not yet been categorized.
    establishment: null,

    // indicates a parking lot or parking structure.
    parking: null,

    // indicates a specific postal box.
    post_box: null,

    // indicates a grouping of geographic areas, such as locality and sublocality, used for mailing addresses in some countries.
    postal_town: null,

    // indicates the room of a building address.
    room: null,

    // indicates the precise street number.
    street_number: null,

    // indicate the location of a bus, train or public transit stop.
    bus_station: null,
    train_station: null,
    transit_station: null
  };

  parseInput = (id, address, location) => {
    this.address.placeID = id;

    if (address) this.parseAddress(address);
    if (location) this.parseGPS(location);
  };

  parseAddress = address => {
    address.forEach(element => {
      let type = element.types[0];
      switch (type) {
        case "country":
        case "postal_code":
          this.address_component[type] = element.short_name;
          break;

        default:
          this.address_component[type] = element.long_name;
      }
    });

    this.address.street =
      this.address_component.street_address ||
      this.address_component.route ||
      "";

    this.address.city =
      this.address_component.sublocality_level_1 ||
      this.address_component.sublocality_level_2 ||
      this.address_component.sublocality_level_3 ||
      this.address_component.sublocality_level_4 ||
      this.address_component.sublocality_level_5 ||
      this.address_component.sublocality ||
      this.address_component.locality ||
      this.address_component.administrative_area_level_1 ||
      this.address_component.administrative_area_level_2 ||
      this.address_component.administrative_area_level_3 ||
      this.address_component.administrative_area_level_4 ||
      this.address_component.administrative_area_level_5 ||
      "";

    this.address.number = this.address_component.premise || "";
    if (
      (this.address_component.premise || "").length > 0 &&
      (this.address_component.street_number || "").length > 0
    )
      this.address.number += "/";
    this.address.number += this.address_component.street_number || "";

    this.address.zipcode = this.address_component.postal_code || "";
    this.address.country = this.address_component.country || "";

    return this.address;
  };

  parseGPS = location => {
    this.address.latitude = location.lat();
    this.address.longitude = location.lng();
  };

  addQuery = query => {
    this.address.search = query;
  };
}

export default Address;
