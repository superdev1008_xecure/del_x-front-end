const addressDTO = {
  id: null,
  search: "",
  street: "",
  houseNr: "",
  city: "",
  zip: "",
  country: "",
  latitude: "",
  longitude: "",
  addressID: "",
  placeID: "",
  countryID: ""
};

export default addressDTO;
