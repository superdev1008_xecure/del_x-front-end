import React from "react";
import addressDTO from "./addressDTO";

const parcelDTO = getEmpty();

function getEmpty() {
  const data = { ...addressDTO };

  data.parcelValueCurrency = "";
  data.parcelType = "";
  data.parcelValue = 0;
  data.parcelPicture = null;
  data.requestedDeliveryTime = "";

  return data;
}

export default parcelDTO;
