import addressDTO from "./addressDTO";
import parcelDTO from "./parcelDTO";

class TransportRequest {
  constructor() {
    this.data = this.inicializeData();
  }

  inicializeData = () => {
    var date = new Date();
    return {
      id: "",
      senderAddressID: "",
      senderCity: "",
      senderStreet: "",
      senderHouseNr: "",
      senderZip: "",
      senderLatitude: 0,
      senderLongitude: 0,
      senderEmail: "",
      senderPhone: "",
      deviceID: "WWW",
      deviceType: 1,
      paymentUrl: null,
      requestedLoadingTime: date,
      items: [
        //{
        //  "id": "string",
        //  "receiverAddressID": "string",
        //  "receiverCity": "string",
        //  "receiverStreet": "string",
        //  "receiverHouseNr": "string",
        //  "receiverZip": "string",
        //  "receiverLatitude": 0,
        //  "receiverLongitude": 0,
        //  "requestedDeliveryTime": "2019-09-06T18:25:56.918Z",
        //  "receiverPhone": "string",
        //  "receiverEmail": "string",
        //  "parcelType": "string",
        //  "parcelValue": 0,
        //  "parcelValueCurrency": "string",
        //  "insuranceRequested": true,
        //  "receiverCountry": "string",
        //  "parcelPicture": "string"
        //}
      ]
    };
  };

  updateState = data => {
    //console.log(      this.data.parcels[0].origin.created.format("yyyy-MM-dd HH:mm:ss")    );
    //console.log(this.data);
  };

  getData = () => {
    return this.data;
  };

  getItem = () => {
    return {
      id: "",
      receiverAddressID: "",
      receiverCity: "",
      receiverStreet: "",
      receiverHouseNr: "",
      receiverZip: "",
      receiverLatitude: 0,
      receiverLongitude: 0,
      receiverPhone: "",
      receiverEmail: "",

      requestedDeliveryTime: null,
      parcelType: "",
      parcelValue: 0,
      parcelValueCurrency: "",
      insuranceRequested: null,
      receiverCountry: "",
      parcelPicture: ""
    };
  };

  addItem = (address, parcel) => {
    const item = { ...parcel };
    item.id = null;
    this.updateAddressData(item, "receiver", address);

    this.data.items.push(item);
  };

  updateSender = address => {
    this.updateAddressData(this.data, "sender", address);
  };

  updateAddressData(data, prefix, address) {
    data[prefix + "AddressID"] = address.addressID;
    data[prefix + "Street"] = address.street;
    data[prefix + "City"] = address.city;
    data[prefix + "HouseNr"] = address.houseNr;
    data[prefix + "Zip"] = address.zip;
    data[prefix + "Latitude"] = address.latitude;
    data[prefix + "Longitude"] = address.longitude;
    data[prefix + "Phone"] = address.phone;
    data[prefix + "Email"] = address.email;
    data[prefix + "Country"] = address.country;
  }
}

function getAddress(data, prefix, defaultValues) {
  const rtn = defaultValues || { ...addressDTO };
  rtn.id = data["id"] || "";
  rtn.addressID = data[prefix + "AddressID"] || "";
  rtn.street = data[prefix + "Street"] || "";
  rtn.city = data[prefix + "City"] || "";
  rtn.houseNr = data[prefix + "HouseNr"] || "";
  rtn.zip = data[prefix + "Zip"] || "";
  rtn.latitude = data[prefix + "Latitude"] || "";
  rtn.longitude = data[prefix + "Longitude"] || "";
  rtn.phone = data[prefix + "Phone"] || "";
  rtn.email = data[prefix + "Email"] || "";
  rtn.country = data[prefix + "Country"] || "";

  rtn.currentLatitude = data["currentLatitude"] || rtn.latitude || "";
  rtn.currentLongitude = data["currentLongitude"] || rtn.longitude || "";

  return rtn;
}

function getParcel(item) {
  const rtn = { ...parcelDTO, ...getAddress(item, "receiver") };
  const columns = ["deliveryOrder", "deliveryStatus", "secretDeliveryCode"];

  transportParameters(columns, item, rtn);
  return rtn;
}

export function transformFromRequest(request) {
  const rtn = { items: [] };

  request = request || rtn;

  rtn.sender = getAddress(request, "sender");

  request.items.map(item => rtn.items.push(getParcel(item)));

  const columns = [
    "id",
    "status",
    "requestedLoadingTime",
    "deviceID",
    "deviceType",
    "paymentUrl",
    "routePolyline",
    "totalDistance",
    "totalPrice",
    "loadAcceptedByOwner",
    "loadAcceptedByTransporter",
    "loadConfirmedByOwner",
    "navigationRouteLink",
    "deliveryOrder",
    "deliveryStatus",
    "secretCode",
    "created"
  ];

  transportParameters(columns, request, rtn);

  copyTransporter(request, rtn);

  return rtn;
}

function transportParameters(list, source, destination) {
  if (list && source && destination)
    list.map(item => transportParameter(source, destination, item));
}

function transportParameter(source, destination, name) {
  destination[name] = source[name];
}

function copyTransporter(source, destination, name = "transporter") {
  destination.transporter = { ...source[name] };
}

function getParcelInfo(source, destination) {
  const columns = ["secretDeliveryCode", "insuranceRequested"];
  transportParameters(columns, source, destination);
}

export function parseParcelDetail(data) {
  console.log("DATA", data);

  const rtn = getAddress(data, "receiver", { ...parcelDTO });

  getParcelInfo(data, rtn);

  copyTransporter(data, rtn, "carriedByTransporter");

  console.log("RTN", rtn);
  return rtn;
}

export default TransportRequest;
