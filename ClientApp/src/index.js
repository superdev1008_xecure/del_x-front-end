import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";

import configureStore from "./store";
import App from "./App";

import registerServiceWorker from "./registerServiceWorker";

import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import "./index.css";

import "jquery/dist/jquery";
import "bootstrap/dist/js/bootstrap";

const rootElement = document.getElementById("root");

ReactDOM.render(
  <Provider store={configureStore()}>
    <App />
  </Provider>,
  rootElement
);

registerServiceWorker();
