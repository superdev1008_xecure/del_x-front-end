import addressDTO from "../../dto/addressDTO";

export const SET_ADDRESS = "SET ADDRESS BY NAME";
export const CLEAR_ADDRESS = "CLEAR ADDRESS";

export const setAddress = (name, data) => {
  const address = { ...addressDTO, ...data };
  return { type: SET_ADDRESS, payload: { name, address } };
};

export const clearAddress = name => {
  return { type: CLEAR_ADDRESS, payload: { name } };
};

export default {
  setAddress,
  clearAddress
};
