export const SET_ADDRESSES = "SET ADDRESSES";

export const setAddresses = addresses => {
  return { type: SET_ADDRESSES, payload: addresses };
};

export default {
  setAddresses
};
