export const SET_CURRENT_REQUEST = "SET CURRENT REQUEST";
export const CLEAR_CURRENT_REQUEST = "CLEAR CURRENT REQUEST";

export const setCurrentRequest = data => {
  return { type: SET_CURRENT_REQUEST, payload: data };
};

export const clearCurrentRequest = () => {
  return { type: CLEAR_CURRENT_REQUEST, payload: null };
};

export default {
  setCurrentRequest,
  clearCurrentRequest
};
