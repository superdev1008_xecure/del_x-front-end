export const SET_COUNTRIES = "SET COUNTRIES";
export const SET_CURRENCIES = "SET CURRENCIES";
export const SET_PARCEL_TYPES = "SET PARCEL TYPES";

export const setCountries = data => {
  return { type: SET_COUNTRIES, payload: data };
};

export const setCurrencies = data => {
  return { type: SET_CURRENCIES, payload: data };
};

export const setParcelTypes = data => {
  return { type: SET_PARCEL_TYPES, payload: data };
};

export default {
  setCountries,
  setCurrencies,
  setParcelTypes
};
