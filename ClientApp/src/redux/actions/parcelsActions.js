export const ADD_PARCEL = "ADD PARCEL";
export const REMOVE_PARCEL = "REMOVE PARCEL";
export const SET_PARCEL_PROPERTY = "SET PARCEL PROPERTY";

export const addParcel = id => {
  return { type: ADD_PARCEL, payload: { id } };
};

export const removeParcel = id => {
  return { type: REMOVE_PARCEL, payload: { id } };
};

export const setParcelProperty = (id, name, data) => {
  return { type: SET_PARCEL_PROPERTY, payload: { id, name, data } };
};

export default {
  addParcel,
  removeParcel,
  setParcelProperty
};
