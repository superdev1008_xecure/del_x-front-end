import { SET_ADDRESSES } from "../actions/addressBookActions";

const defaultState = { loaded: false, addresses: [] };

export default (state = defaultState, action) => {
  const { type, payload } = action;
  const rtn = { ...state };

  //console.log("ADDRESSBOOK_REDUCER", action);

  switch (type) {
    case SET_ADDRESSES:
      rtn.loaded = true;
      rtn.addresses = payload;
      break;

    default:
      break;
  }

  //  console.log("ADDRESS_BOOK_STATE", rtn);
  return rtn;
};
