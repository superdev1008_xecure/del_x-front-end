import { SET_ADDRESS, CLEAR_ADDRESS } from "../actions/addressActions";

const defaultState = {};

export default (state = defaultState, action) => {
  const { type, payload } = action;
  const rtn = { ...state };

  //console.log("ADDRESS_REDUCER", action);

  switch (type) {
    case SET_ADDRESS:
      rtn[payload.name] = payload.address;
      break;

    case CLEAR_ADDRESS:
      if (payload.name !== undefined) rtn[payload.name] = {};
      break;

    default:
      break;
  }

  //console.log("ADDRESSES_STATE", rtn);
  return rtn;
};
