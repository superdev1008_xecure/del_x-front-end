import {
  SET_CURRENT_REQUEST,
  CLEAR_CURRENT_REQUEST
} from "../actions/currentRequestActions";

const defaultState = {};

export default (state = defaultState, action) => {
  const { type, payload } = action;
  let rtn = { ...state };

  //console.log("CURRENT_REQUEST_STATE", action);

  switch (type) {
    case SET_CURRENT_REQUEST:
      rtn = payload;
      break;

    case CLEAR_CURRENT_REQUEST:
      rtn = defaultState;
      break;

    default:
      break;
  }

  //console.log("CURRENT_REQUEST_STATE", rtn);
  return rtn;
};
