import {
  SET_COUNTRIES,
  SET_CURRENCIES,
  SET_PARCEL_TYPES
} from "../actions/enumsActions";

const defaultState = {
  countries: {
    loaded: false,
    data: []
  },
  currencies: {
    loaded: false,
    data: []
  },
  parcelTypes: {
    loaded: false,
    data: []
  }
};

export default (state = defaultState, action) => {
  const { type, payload } = action;
  const rtn = { ...state };

  switch (type) {
    case SET_COUNTRIES:
      rtn.countries.loaded = true;
      rtn.countries.data = payload;
      break;

    case SET_CURRENCIES:
      rtn.currencies.loaded = true;
      rtn.currencies.data = payload;
      break;

    case SET_PARCEL_TYPES:
      rtn.parcelTypes.loaded = true;
      rtn.parcelTypes.data = payload;
      break;

    default:
      break;
  }

  return rtn;
};
