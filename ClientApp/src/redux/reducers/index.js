import { combineReducers } from "redux";
import parcelsReducer from "./parcelsReducer";
import addressesReducer from "./addressReducer";
import addressBookReducer from "./addressBookReducer";
import enumsReducer from "./enumsReducer";
import currentRequestReducer from "./currentRequestReducer";

export default combineReducers({
  addresses: addressesReducer,
  parcels: parcelsReducer,
  addressBook: addressBookReducer,
  enums: enumsReducer,
  currentRequest: currentRequestReducer
});
