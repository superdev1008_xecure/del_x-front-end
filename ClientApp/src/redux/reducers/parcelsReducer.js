import {
  ADD_PARCEL,
  REMOVE_PARCEL,
  SET_PARCEL_PROPERTY
} from "../actions/parcelsActions";
import parcelDTO from "../../dto/parcelDTO";

const defaultState = [];

export default (state = defaultState, action) => {
  const { type, payload } = action;

  let rtn = [...state];

  switch (type) {
    case ADD_PARCEL:
      const item = { ...parcelDTO, ...payload };
      rtn.push(item);
      break;

    case REMOVE_PARCEL:
      rtn = rtn.filter(m => m.id !== payload.id);
      break;

    case SET_PARCEL_PROPERTY:
      const parcel = rtn.filter(m => m.id === payload.id)[0];
      if (parcel) {
        parcel[payload.name] = payload.data;
      }
      break;

    default:
      break;
  }

  //console.log("PARCELS_STATE", rtn);
  return rtn;
};
