import http from "./httpService";

const apiEndpoints = {
  getAddresses: "/api/request/GetAllAddressBookItems",
  addAddress: "/api/request/CreateAddressBookItems",
  removeAddress: "/api/request/DeleteAddressBookItems"
};

export function getAddresses() {
  return http.get(apiEndpoints["getAddresses"]);
}

export function addAddress(address) {
  return http.post(apiEndpoints["addAddress"], [address]);
}

export function removeAddress(id) {
  return http.post(apiEndpoints["removeAddress"], [id]);
}

export default {
  getAddresses,
  addAddress,
  removeAddress
};
