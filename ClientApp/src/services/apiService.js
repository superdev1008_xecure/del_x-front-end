import http from "./httpService";

const apiEndpoints = {
  getCountries: "/api/CodeList/GetAllCountries",
  getParcelTypes: "/api/CodeList/GetAllParcelTypes",
  getCurrencies: "/api/CodeList/GetAllCurrencies",

  getAddresses: "/api/request/GetAllAddressBookItems",

  addAddress: "/api/request/CreateAddressBookItems",
  removeAddress: "/api/request/DeleteAddressBookItems",
  sendTransportRequest: "/api/request/RegisterNewRequest",
  getTransportRequests: "/api/Request/GetAllTransportRequests",
  getRequest: "/api/Request/GetRequest",
  getParcelDetail: "/api/Request/GetParcelDetail",

  acceptTransportRequestByClient: "/api/Request/AcceptTransportRequestByClient",
  acceptTransportRequest: "/api/Transporter/AcceptTransportRequest",

  transporterUpdateLastLocation: "/api/Transporter/UpdateLastLocation",
  transporterPackagesLoaded: "/api/Transporter/PackagesLoaded",
  transporterPackageDelivered: "/api/Transporter/PackageDelivered",

  loadConfirmedByOwner: "/api/Request/LoadConfirmedByOwner",
  getAllUnacceptedByTransporter:
    "/api/Transporter/GetAllUnacceptedByTransporter",
  getAllOwnUndeliveredTransportRequests:
    "/api/Transporter/GetAllOwnUndeliveredTransportRequests",

  declineTransportRequest: "/api/Transporter/RequestRejected/"
};

export function getCountries() {
  return http.get(apiEndpoints["getCountries"]);
}

export function getParcelTypes() {
  return http.get(apiEndpoints["getParcelTypes"]);
}

export function getCurrencies() {
  return http.get(apiEndpoints["getCurrencies"]);
}

export function getAddresses() {
  return http.get(apiEndpoints["getAddresses"]);
}

export function addAddress(address) {
  return http.post(apiEndpoints["addAddress"], [address]);
}

export function removeAddress(id) {
  return http.post(apiEndpoints["removeAddress"], [id]);
}

export function sendTransportRequest(request) {
  return http.post(apiEndpoints["sendTransportRequest"], request);
}
export function getTransportRequests() {
  return http.post(apiEndpoints["getTransportRequests"]);
}

export function getRequest(id) {
  return http.get(apiEndpoints["getRequest"] + "/" + id);
}

export function getParcelDetail(request, parcel) {
  return http.get(
    apiEndpoints["getParcelDetail"] + "/" + request + "/" + parcel
  );
}

export function declineTransportRequest(id) {
  return http.get(apiEndpoints["declineTransportRequest"] + "/" + id);
}

export function acceptTransportRequest(id) {
  return http.get(apiEndpoints["acceptTransportRequest"] + "/" + id);
}

export function acceptTransportRequestByClient(id) {
  return http.get(
    apiEndpoints["acceptTransportRequestByClient"] + "?RequestID=" + id
  );
}

export function transporterUpdateLastLocation(latitude, longitude) {
  return http.get(
    apiEndpoints["transporterUpdateLastLocation"] +
      "/" +
      latitude +
      "/" +
      longitude
  );
}

export function transporterPackagesLoaded(id) {
  return http.get(apiEndpoints["transporterPackagesLoaded"] + "/" + id);
}

export function loadConfirmedByOwner(id) {
  return http.get(apiEndpoints["loadConfirmedByOwner"] + "/" + id);
}

export function transporterPackageDelivered(id) {
  return http.get(apiEndpoints["transporterPackageDelivered"] + "/" + id);
}

export function getAllUnacceptedByTransporter() {
  return http.get(apiEndpoints["getAllUnacceptedByTransporter"]);
}

export function getAllOwnUndeliveredTransportRequests() {
  return http.get(apiEndpoints["getAllOwnUndeliveredTransportRequests"]);
}

export default {
  getCountries,
  getParcelTypes,
  getCurrencies,
  getAddresses,
  addAddress,
  removeAddress,
  sendTransportRequest,
  getTransportRequests,
  getRequest,
  getParcelDetail,
  declineTransportRequest,
  acceptTransportRequest,
  acceptTransportRequestByClient,
  transporterUpdateLastLocation,
  transporterPackagesLoaded,
  transporterPackageDelivered,
  loadConfirmedByOwner,
  getAllUnacceptedByTransporter,
  getAllOwnUndeliveredTransportRequests
};
