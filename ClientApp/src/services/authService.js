import http from "./httpService";
import * as jwt_decode from "jwt-decode";

const apiEndpoint = "/users/authenticate/";
const tokenKey = "token";

http.setJWT(getJWT());

export async function login(email, password) {
  const { data: user } = await http.post(apiEndpoint, { email, password });
  setJWT(user.token);
}

export function loginWithJWT(jwt) {
  setJWT(jwt);
}

export function logout() {
  localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
  try {
    const jwt = getJWT();
    const user = jwt_decode(jwt);
    user.transporter = user.IsTransporter === "1";
    return user;
  } catch (error) {
    //console.log(error);
    return null;
  }
}

function setJWT(value) {
  return localStorage.setItem(tokenKey, value);
}

function getJWT() {
  return localStorage.getItem(tokenKey);
}

export default {
  login,
  logout,
  getCurrentUser,
  loginWithJWT,
  getJWT
};
