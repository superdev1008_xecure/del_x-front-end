import * as Sentry from "@sentry/browser";

function init() {
  Sentry.init({
    dsn: "https://d3dc685beeca43bc81c9b0610d660da1@sentry.io/1524324"
  });
}

function log(error) {
  Sentry.captureException(error);
}

export default {
  init,
  log
};
