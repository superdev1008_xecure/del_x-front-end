import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import reducer from "./redux/reducers/index";

const composeEnhancers = composeWithDevTools({
  // Specify custom devTools options
});

function configureStore(state) {
  return createStore(
    reducer,
    state,
    composeEnhancers()
    //applyMiddleware(...middleware)
    // other store enhancers if any
  );
}

export default configureStore;
